-- Consulta de un tabla completa
select * from materiales
-- Selección
select * from materiales
where clave=1000
-- Proyección
select clave,rfc,fecha from entregan
-- Join Natural
select * from materiales,entregan
where materiales.clave = entregan.clave
-- Teta Join
select * from entregan,proyectos
where entregan.numero <= proyectos.numero
-- Union
select * from entregan where clave=1450 or clave=1300
-- Intersección
select clave from entregan where numero=5001 and numero=5018
-- Diferencia
select * from entregan where clave != 1000
-- Producto cartesiano
select * from entregan,materiales
-- Construcción de consultas a partir de una especificación
set dateformat dmy
SELECT m.Descripcion
FROM Materiales m, Entregan e
WHERE m.Clave = e.Clave AND (e.Fecha >= 01/01/2000 OR e.Fecha < 01/01/2001)
-- Distinct
SELECT DISTINCT m.Descripcion
FROM Materiales m, Entregan e
WHERE m.Clave = e.Clave AND (e.Fecha >= 01/01/2000 OR e.Fecha < 01/01/2001)
-- Ordenamientos
SELECT p.Numero, p.Denominacion, e.Fecha, e.Cantidad
FROM Proyectos p, Entregan e
WHERE p.Numero = e.Numero
ORDER BY p.Numero, e.Fecha DESC
-- Operadores de cadena
SELECT * FROM Materiales where Descripcion LIKE 'Si%'
-- Más operadores de cadena
DECLARE @foo varchar(40);
DECLARE @bar varchar(40);
SET @foo = '¿Que resultado';
SET @bar = ' ¿¿¿??? '
SET @foo += ' obtienes?';
PRINT @foo + @bar;
-- Más operadores de cadena
SELECT RFC FROM Entregan WHERE RFC LIKE '[A-D]%';
-- Más operadores de cadena
SELECT RFC FROM Entregan WHERE RFC LIKE '[^A]%';
-- Más operadores de cadena
SELECT Numero FROM Entregan WHERE Numero LIKE '___6';
-- Operadores lógicos
SELECT Clave,RFC,Numero,Fecha,Cantidad
FROM Entregan
WHERE Numero Between 5000 and 5010;
-- Exists
SELECT RFC,Cantidad, Fecha,Numero
FROM [Entregan]
WHERE [Numero] Between 5000 and 5010 AND
Exists ( SELECT [RFC]
FROM [Proveedores]
WHERE RazonSocial LIKE 'La%' and [Entregan].[RFC] = [Proveedores].[RFC] )
-- In
SELECT RFC,Cantidad, Fecha,Numero
FROM Entregan
WHERE Numero Between 5000 and 5010 AND RFC
IN ( SELECT RFC
FROM Proveedores
WHERE RazonSocial LIKE 'La%' and Entregan.RFC = Proveedores.RFC )
-- Not in
SELECT RFC,Cantidad, Fecha,Numero
FROM Entregan
WHERE Numero Between 5000 and 5010 AND RFC
NOT IN ( SELECT RFC
FROM Proveedores
WHERE RazonSocial NOT LIKE 'La%' and Entregan.RFC = Proveedores.RFC )
-- Operadores all, some, any
SELECT RFC,Cantidad
FROM Entregan
WHERE RFC = ANY ( SELECT RFC
FROM Proveedores
WHERE RazonSocial LIKE 'La%' and Entregan.RFC = Proveedores.RFC )
-- Top
SELECT TOP 2 * FROM Proyectos
-- Modificando la estructura de un tabla existente.
ALTER TABLE Materiales ADD PorcentajeImpuesto NUMERIC(6,2);
UPDATE materiales SET PorcentajeImpuesto = 2*clave/1000;
-- Obtener el importe de las entregas es decir basado en la cantidad de la entrega y el precio del material y el impuesto asignado
SELECT e.Clave, e.RFC, e.Numero, e.Fecha, e.Cantidad, ((m.PorcentajeImpuesto/100)+1)*m.Costo*e.Cantidad as 'Total'
FROM Entregan e, Materiales m
WHERE e.Clave = m.Clave
-- Creación de vistas
CREATE VIEW Primeros2Proyectos (NumProy, DenProy)
AS SELECT TOP 2 * FROM Proyectos
-- Ver vista
SELECT * FROM Primeros2Proyectos
-- Crear vistas para 5 consultas
  -- 1
CREATE VIEW  Entregas5000a5010deLa (RFCProv, CantEnt, FechaEnt,NumProy)
AS SELECT RFC,Cantidad, Fecha,Numero
FROM Entregan
WHERE Numero Between 5000 and 5010 AND RFC
IN ( SELECT RFC
FROM Proveedores
WHERE RazonSocial LIKE 'La%' and Entregan.RFC = Proveedores.RFC )
SELECT * FROM Entregas5000a5010deLa
  -- 2
CREATE VIEW MaterialesEntregados (Clave, Descr)
AS select materiales.Clave, Materiales.Descripcion from materiales,entregan
where materiales.clave = entregan.clave
SELECT * FROM MaterialesEntregados
  -- 3
CREATE VIEW MaterialesClaveNo1000 (ClaveMat, RFCProv, NumProy, FechaEnt, CantEnt)
AS select * from entregan where clave != 1000
SELECT * FROM MaterialesClaveNo1000
  -- 4
CREATE VIEW MaterialesClave1000 (ClaveMat, RFCProv, NumProy, FechaEnt, CantEnt)
AS select * from entregan where clave = 1000
SELECT * FROM MaterialesClave1000
  -- 5
CREATE VIEW Primeros4Proyectos (NumProy, DenProy)
AS SELECT TOP 4 * FROM Proyectos
SELECT * FROM Primeros4Proyectos

-- Consultas
  -- 1
SELECT m.Clave, m.Descripcion
FROM Materiales m, Entregan e, Proyectos p
WHERE m.Clave=e.Clave AND p.Numero=e.Numero AND p.Denominacion='Mexico sin ti no estamos completos'
/*
Muestra:
1030	Varilla 4/33
1230	Cemento
Num tuplas: 3
*/
  -- 2
SELECT m.Clave, m.Descripcion
FROM Materiales m, Entregan e, Proveedores p
WHERE m.Clave=e.Clave AND p.RFC=e.RFC AND p.RazonSocial='Acme tools'
/*
Muestra:
Num tuplas: 0
 */
  -- 3
SET DATEFORMAT dmy
SELECT p.RFC
FROM Proveedores p, Entregan e
WHERE p.RFC=e.RFC AND (e.Fecha BETWEEN '01/01/2000' AND '31/12/2000')
GROUP BY p.RFC
HAVING AVG(e.Cantidad) >= 300
/*
Muestra:
BBBB800101
CCCC800101
NumTuplas: 7
 */
 -- 4
SET DATEFORMAT dmy
SELECT m.Clave, SUM(e.Cantidad)
FROM Entregan e, Materiales m
WHERE e.Clave=m.Clave AND (e.Fecha BETWEEN '01/01/2000' AND '31/12/2000')
GROUP BY m.Clave
/*
Muestra:
1000	7.00
1010	1195.00
NumTuplas: 22
 */
 -- 5
SET DATEFORMAT dmy
SELECT TOP 1 Clave, SUM(e.Cantidad) as 'Total'
FROM Entregan e
WHERE e.Fecha BETWEEN '01/01/2001' AND '31/12/2001'
GROUP BY e.Clave
ORDER BY 'Total' DESC
/*
Muestra:
1020	1060.00
NumTuplas: 1
 */
 -- 6
SELECT m.Clave, m.Descripcion
FROM Materiales m
WHERE m.Descripcion LIKE '%ub%'
/*
Muestra:
1180	Recubrimiento P1001
1190	Recubrimiento P1010
NumTuplas: 12
 */
  -- 7
SELECT p.Denominacion, SUM((m.Costo*e.Cantidad)*(1+m.PorcentajeImpuesto/100)) AS 'Total a pagar'
FROM Proyectos p, Entregan e, Materiales m
WHERE p.Numero=e.Numero AND m.Clave=e.Clave
GROUP BY p.Denominacion
/*
Muestra:
Ampliación de la carretera a la huasteca	742461.1940000000
Aztecón	150200.2190000000
NumTuplas: 20
 */
  -- 8
CREATE VIEW ProvTel
AS SELECT DISTINCT prov.RFC, prov.RazonSocial
FROM Proyectos p, Entregan e, Proveedores prov
WHERE p.Numero=e.Numero AND prov.RFC=e.RFC AND p.Denominacion='Televisa en acción'

CREATE VIEW ProvEdu
AS SELECT DISTINCT prov.RFC, prov.RazonSocial
FROM Proyectos p, Entregan e, Proveedores prov
WHERE p.Numero=e.Numero AND prov.RFC=e.RFC AND p.Denominacion='Educando en Coahuila'

CREATE VIEW ProvNoEdu
AS SELECT DISTINCT prov.RFC, prov.RazonSocial
FROM Proyectos p, Entregan e, Proveedores prov
WHERE p.Numero=e.Numero AND prov.RFC=e.RFC AND p.Denominacion!='Educando en Coahuila'

SELECT pt.RFC, pt.RazonSocial
FROM ProvTel pt
LEFT JOIN ProvEdu pe ON pe.RFC = pt.RFC
WHERE pe.RFC IS NULL
/*
Muestra:
CCCC800101   	La Ferre
DDDD800101   	Cecoferre
NumTuplas: 2
 */
  -- 9
SELECT DISTINCT p.Denominacion, prov.RFC, prov.RazonSocial
FROM Proyectos p, Proveedores prov, Entregan e
WHERE e.RFC=prov.RFC AND e.Numero=p.Numero AND p.Denominacion='Televisa en acción' AND prov.RFC NOT IN (
  SELECT prov.RFC
  FROM Proveedores prov, Proyectos p, Entregan e
  WHERE prov.RFC = e.RFC AND p.Numero = e.Numero AND p.Denominacion = 'Educando en Coahuila'
  )
/*
Muestra:
Televisa en acción	CCCC800101   	La Ferre
Televisa en acción	DDDD800101   	Cecoferre
NumTuplas: 2
 */
  -- 10
SELECT m.Descripcion, m.Costo
FROM Materiales m, Entregan e, Proveedores prov, Proyectos p
WHERE e.Numero = p.Numero AND m.Clave = e.Clave AND e.RFC = prov.RFC AND p.Denominacion = 'Televisa en acción' AND prov.RFC IN (
  SELECT prov.RFC
  FROM Proveedores prov, Entregan e, Proyectos p
  WHERE e.RFC = prov.RFC AND e.Numero = p.Numero  AND p.Denominacion = 'Educando en Coahuila'
  )
/*
Muestra:
Ladrillos rojos	50.00
Tepetate	34.00
NumTuplas: 2
 */
  -- 11
SELECT m.Descripcion, COUNT(*) as 'Num entregas', SUM((e.Cantidad*m.Costo)*(1+m.PorcentajeImpuesto/100)) as 'Costo total'
FROM Materiales m, Entregan e
WHERE m.Clave = e.Clave
GROUP BY m.Descripcion
/*
Muestra:
Arena	3	218692.3200000000
Block	3	51754.0800000000
NumTuplas: 42
 */