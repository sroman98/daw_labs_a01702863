--Crear tablas
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Materiales')
drop table Materiales
CREATE TABLE Materiales
(
  Clave numeric(5) not null,
  Descripcion varchar(50),
  Costo numeric(8,2)
)

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proveedores')
drop table Proveedores
CREATE TABLE Proveedores
(
  RFC char(13) NOT NULL,
  RazonSocial varchar(50)
)

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proyectos')
drop table Proyectos
CREATE TABLE Proyectos
(
  Numero numeric(5) not null,
  Denominacion varchar(50)
)

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Entregan')
drop table Entregan
CREATE TABLE Entregan
(
  Clave numeric(5) NOT NULL,
  RFC char(13) NOT NULL,
  Numero numeric(5) NOT NULL,
  Fecha datetime NOT NULL,
  Cantidad numeric(8,2)
)

-- Cargar datos
BULK INSERT a1702863.a1702863.[Materiales] 
  FROM 'e:\wwwroot\a1702863\materiales.csv'
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n'
  )

BULK INSERT a1702863.a1702863.[Proyectos] 
  FROM 'e:\wwwroot\a1702863\Proyectos.csv'
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n'
  ) 

BULK INSERT a1702863.a1702863.[Proveedores] 
  FROM 'e:\wwwroot\a1702863\Proveedores.csv'
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n'
  ) 

SET DATEFORMAT dmy -- especificar formato de la fecha 

BULK INSERT a1702863.a1702863.[Entregan] 
  FROM 'e:\wwwroot\a1702863\Entregan.csv'
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n'
  ) 