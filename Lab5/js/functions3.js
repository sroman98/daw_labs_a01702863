//EXERCISE 3
email1 = document.getElementById('email1');

email2 = document.getElementById('email2');
email2.oninput = checkEmails;

submit = document.getElementById('submit');
submit.onclick = function() {
  if(checkEmails() && validateEmail(email1)) {
    alert("¡ÉXITO! \nSe te ha enviado un correo para restablecer tu contraseña, procura no olvidar la nueva. Salu2.");
    location.reload();
  }
  else if(!validateEmail(email1)){
    alert("El correo que ingresaste NO ES VÁLIDO, intenta de nuevo.");
    return;
  }
  else {
    alert("Los dos correos deben COINCIDIR, intenta de nuevo.");
    return;
  }
}

function checkEmails() {
  //verificar que sean iguales los correos
  if(email2.value != email1.value) {
    document.getElementById('error').style.display = "block";
    return false;
  }
  else {
    document.getElementById('error').style.display = "none";
    return true;
  }
}

function validateEmail(email) {
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(email.value)) {
      return false;
    }
    return true;
}
