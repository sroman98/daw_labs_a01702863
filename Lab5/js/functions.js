document.getElementById('botonInfoUsuario').onmouseover = function() {showExtraLoginInfo(document.getElementById('pInfoUsuario'))};
document.getElementById('botonInfoContrasena').onmouseover = function() {showExtraLoginInfo(document.getElementById('pInfoContrasena'))};

document.getElementById('botonInfoUsuario').onmouseout = function() {hideExtraLoginInfo(document.getElementById('pInfoUsuario'))};
document.getElementById('botonInfoContrasena').onmouseout = function() {hideExtraLoginInfo(document.getElementById('pInfoContrasena'))};

function showExtraLoginInfo(paragraphToShow) {
  paragraphToShow.style.display = "block";
}

function hideExtraLoginInfo(paragraphToShow) {
  paragraphToShow.style.display = "none";
}

pwd1 = document.getElementById('pwd1');
pwd2 = document.getElementById('pwd2');
user = document.getElementById('username');
form = document.getElementById('form');

pwd1.oninput = verifyPassword;
pwd2.oninput = checkPasswords;
user.oninput = verifyUser;
form.onsubmit = verifyAll;

errors = ['true', 'true', 'true', 'true', 'true', 'true'];
errorsString = ["- El usuario debe tener más de 5 caracteres", "- La contraseña debe tener más de 8 caracteres", "- La contraseña debe tener al menos 1 número", "- La contraseña debe tener al menos 1 caracter especial", "- La contraseña debe tener al menos 1 mayúscula", "- Ambas contraseñas deben coincidir"];

function verifyAll() {
  var errorMessage = "";
  var isvalid = true;
  for (var i = 0; i < errors.length; i++) {
    if(errors[i]) {
      errorMessage += errorsString[i] + "\n";
      isvalid = false;
    }
  }
  if(isvalid) {
    window.alert("¡Te has registrado exitosamente!");
  }
  else
    window.alert(errorMessage);
}

function verifyPassword() {
  //verificar el tamaño
  if(verifySize(8,pwd1.value)) {
    document.getElementById('error2').style.display = "block";
    errors[1] = true;
  }
  else {
    document.getElementById('error2').style.display = "none";
    errors[1] = false;
  }

  //verificar que haya números
  verifyNumber();
  //verificar que haya caracteres especiales
  verifySpecialChars();
  //verificar que haya mayúsculas
  verifyUpperCase();
}

function checkPasswords() {
  //verificar que sean iguales las contraseñas
  if(pwd2.value != pwd1.value) {
    document.getElementById('error6').style.display = "block";
    errors[5] = true;
  }
  else {
    document.getElementById('error6').style.display = "none";
    errors[5] = false;
  }
}

function verifyUser() {
  //verificar el tamaño
  if(verifySize(5,user.value)) {
    document.getElementById('error1').style.display = "block";
    errors[0] = true;
  }
  else {
    document.getElementById('error1').style.display = "none";
    errors[0] = false;
  }
}

function verifySize(minSize, string) {
  if (string.length < minSize)
    return true;
  else
    return false;
}
function verifyNumber() {
  var numbers = /[0-9]/g;
  if(pwd1.value.match(numbers)) {
    document.getElementById('error3').style.display = "none";
    errors[2] = false;
  } else {
    document.getElementById('error3').style.display = "block";
    errors[2] = true;
  }
}
function verifySpecialChars() {
  var chars = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
  if(pwd1.value.match(chars)) {
    document.getElementById('error4').style.display = "none";
    errors[3] = false;
  } else {
    document.getElementById('error4').style.display = "block";
    errors[3] = true;
  }
}
function verifyUpperCase() {
  var upperCaseLetters = /[A-Z]/g;
  if(pwd1.value.match(upperCaseLetters)) {
    document.getElementById('error5').style.display = "none";
    errors[4] = false;
  } else {
    document.getElementById('error5').style.display = "block";
    errors[4] = true;
  }
}
