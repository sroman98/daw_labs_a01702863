SELECT DISTINCT m.Descripcion
FROM Materiales m, Entregan e, Proyectos p
WHERE m.Clave=e.Clave AND p.Numero=e.Numero AND p.Denominacion='Teletón Qro'
ORDER BY m.Descripcion ASC
/* lo prové con denominación 'Infonavit Durango' el ASC es opcional, lo pongo por claridad*/

SET DATEFORMAT dmy
SELECT p.Denominacion, SUM(e.Cantidad) as 'Total'
FROM Proyectos p, Entregan e
WHERE p.Numero=e.Numero AND e.Fecha BETWEEN '01/01/2010' AND '31/12/2010'
GROUP BY p.Denominacion
ORDER BY Total DESC
/* lo prové con año 2001 */

SELECT m.Clave, m.Descripcion
FROM Materiales m
WHERE m.Clave NOT IN (
  SELECT e.Clave
  FROM Entregan e
  )