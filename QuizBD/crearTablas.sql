IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Carreras')
drop table Carreras
CREATE TABLE Carreras
(
  codigo varchar(5) not null,
  nombre varchar(50) not null

  constraint llaveCarreras PRIMARY KEY (codigo)
)

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Alumnos')
drop table Alumnos
CREATE TABLE Alumnos
(
  matricula varchar(8) not null,
  nombre varchar(30) not null,
  apellidoP varchar(30) not null,
  apellidoM varchar(30),
  codigo varchar(5) not null

  constraint llaveAlumnos PRIMARY KEY (matricula)
  constraint fkAlumnos foreign key (codigo) references Carreras(codigo)
)