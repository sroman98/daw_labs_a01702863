INSERT INTO Carreras (codigo, nombre) VALUES ('ISC','Ingeniería en Sistemas Computacionales')
INSERT INTO Carreras (codigo, nombre) VALUES ('ISDR','Ingeniería en Sistemas Digitales y Robótica')

INSERT INTO Alumnos (matricula, nombre, apellidoP, apellidoM, codigo) VALUES (1702863, 'Sandra', 'Román', 'Rivera', 'ISC')
INSERT INTO Alumnos (matricula, nombre, apellidoP, apellidoM, codigo) VALUES (1703123, 'Michael', 'Jordan', NULL, 'ISDR')