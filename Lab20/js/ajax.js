function getRequestObject() {
  // Asynchronous objec created, handles browser DOM differences

  if(window.XMLHttpRequest) {
    // Mozilla, Opera, Safari, Chrome IE 7+
    return (new XMLHttpRequest());
  }
  else if (window.ActiveXObject) {
    // IE 6-
    return (new ActiveXObject("Microsoft.XMLHTTP"));
  } else {
    // Non AJAX browsers
    return(null);
  }
}

function sendRequest() {
  var jq = 1;

  if(jq) {
    $.get( "ssajax.php", { pattern: document.getElementById('userInput').value })
         .done(function( data ) {
             var ajaxResponse = document.getElementById('ajaxResponse');
             ajaxResponse.innerHTML = data;
             ajaxResponse.style.visibility = "visible";
             M.AutoInit();
         });
  }
  else {
    request=getRequestObject();
    if(request!=null) {
      var userInput = document.getElementById('userInput');
      var url='ssajax.php?pattern='+userInput.value;
      request.open('GET',url,true);
      request.onreadystatechange =
             function() {
                 if((request.readyState==4)){
                     // Asynchronous response has arrived
                     var ajaxResponse=document.getElementById('ajaxResponse');
                     ajaxResponse.innerHTML=request.responseText;
                     ajaxResponse.style.visibility="visible";
                     //M.AutoInit();
                 }
             };
      request.send(null);
    }
  }
}

function sendRequest2() {
  $.get( "ssajax.php", { numDebt: document.getElementById('userInput2').value })
       .done(function( data ) {
           var ajaxResponse = document.getElementById('ajaxResponse2');
           ajaxResponse.innerHTML = data;
           ajaxResponse.style.visibility = "visible";
           M.AutoInit();
       });
}


function selectValue() {

   var list=document.getElementById("list");
   var userInput=document.getElementById("userInput");
   var ajaxResponse=document.getElementById('ajaxResponse');
   userInput.value=list.options[list.selectedIndex].text;
   ajaxResponse.style.visibility="hidden";
   userInput.focus();
}
