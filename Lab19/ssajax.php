<?php
  require_once "util.php";

  if(isset($_GET['pattern'])) {
    $pattern=strtolower($_GET['pattern']);
    if($pattern != "") {
      $words=getWordsByName($pattern);
      $size = mysqli_num_rows($words);
      if($size > 0) {
        while($word = mysqli_fetch_assoc($words)) {
          $response.="<option value='".$word['Name']."'>".$word['Name']."</option>";
        }
        echo "<select id='list' size=".$size." onclick='selectValue()'>".$response."</select>";
      }
      else {
        echo "No results were found with pattern: ".$_GET['pattern'];
      }
    }
  }
  else if(isset($_GET['numDebt'])) {
    $debt=$_GET['numDebt'];
    if($debt != "") {
      $clients=getClientsByDebt($debt);
      $size = mysqli_num_rows($clients);
      if($size > 0) {
        while($client = mysqli_fetch_assoc($clients)) {
          $response.="<option value='".$client['Id']."'>".$client['Nombre']." ".$client['Apellido']." $".$client['Deuda']."</option>";
        }
        echo "<select id='list' size=".$size." onclick='selectValue()'>".$response."</select>";
      }
      else {
        echo "No results were found with pattern: ".$_GET['numDebt'];
      }
    }
  }

?>
