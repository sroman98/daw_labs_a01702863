<?php
  function connectDB() {
    $servername = "localhost";
    $username = "sroman98";
    $password = "s123";
    $dbname = "lab19";

    $con = mysqli_connect($servername, $username, $password, $dbname);
    // Check fann_get_total_connection
    if(!$con) {
      die("Connection failed: ".mysqli_connect_error());
    }

    return $con;
  }

  function closeDB($con) {
    mysqli_close($con);
  }

  function getWordsByName($hint) {
    $con = connectDB();

    $sql = "SELECT Name FROM Words WHERE Name LIKE '%".$hint."%'";

    $result = mysqli_query($con, $sql);

    closeDB($con);

    return $result;
  }

  function getClientsByDebt($debt) {
    $con = connectDB();

    $sql = "SELECT * FROM Clients WHERE Deuda >= ".$debt." ORDER BY Deuda ASC";

    $result = mysqli_query($con, $sql);

    closeDB($con);

    return $result;
  }
?>
