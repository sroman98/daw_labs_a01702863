-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Mar 20, 2019 at 12:47 AM
-- Server version: 5.7.25
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `lab19`
--

-- --------------------------------------------------------

--
-- Table structure for table `Clients`
--

CREATE TABLE `Clients` (
  `Id` int(11) NOT NULL,
  `Nombre` varchar(40) NOT NULL,
  `Apellido` varchar(40) NOT NULL,
  `Deuda` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Clients`
--

INSERT INTO `Clients` (`Id`, `Nombre`, `Apellido`, `Deuda`) VALUES
(1, 'Margarita', 'Zavala', 4000000),
(2, 'Andrés Manuel', 'López', 135000000),
(3, 'Gabriel', 'Quadri', 6000),
(4, 'Vicente', 'Fox', 98000),
(5, 'Ricardo', 'Anaya', 1500),
(6, 'Qrolina', 'Nyquist', 15),
(7, 'Ludwig', 'Van Beethoven', 120),
(8, 'Justin', 'Bieber', 0);

-- --------------------------------------------------------

--
-- Table structure for table `Words`
--

CREATE TABLE `Words` (
  `Id` int(11) NOT NULL,
  `Name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Words`
--

INSERT INTO `Words` (`Id`, `Name`) VALUES
(2, 'AJAX'),
(5, 'Data bases'),
(1, 'Distributed applications'),
(3, 'Java Server Pages'),
(6, 'JavaScript'),
(7, 'Networking'),
(8, 'PHP'),
(4, 'Web services');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Clients`
--
ALTER TABLE `Clients`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `Words`
--
ALTER TABLE `Words`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Unique` (`Name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Clients`
--
ALTER TABLE `Clients`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `Words`
--
ALTER TABLE `Words`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
