//EJERCICIO 1
function ask1Num(num) {
  var x = prompt("Enter a number:", "10");
  if(num==1)
    ex1(x);
  else
    ex5(x);
}

function ex1 (num) {
  if(num<1) {
    window.alert("Sorry, no puedo hacer eso, bro.");
  }

  else {
    var  string = "<br><table><tr><th>n</th><th>n^2</th><th>n^3</th></tr>";
    for (var i = 0; i < num; i++) { //row
      string+="<tr>";
      string+="<td>"+(i+1)+"</td>";
      string+="<td>"+((i+1)*(i+1))+"</td>";
      string+="<td>"+((i+1)*(i+1)*(i+1))+"</td>";
      string+="</tr>";
    }
    string+="</table><br>";

    var div = document.getElementById('resultsE1');
    div.innerHTML = string;
  }
}

//EJERCICIO2
function askSumNum() {
  //se generan dos números aleatorios entre 1 y 100
  random1 = Math.floor((Math.random() * 100) + 1);
  random2 = Math.floor((Math.random() * 100) + 1);

  prevDate = new Date();
  var x = prompt("Enter the sum of "+random1+" and "+random2, "0");
  currDate = new Date();
  ex2(x);
}

function ex2 (res) {
  if(random1 + random2 == res) {
    window.alert("¡CORRECTO! Solo tardaste "+((currDate-prevDate)/1000)+" segundos en responder.");
  }
  else {
    window.alert("¡INCORRECTO! Y aún así tardaste "+((currDate-prevDate)/1000)+" segundos en responder.");
  }
}

//EJERCICIO3
function askArray() {
  var n, nums = [], x;

  n = prompt("¿Cuántos números vas a ingresar?", "3");

  for (var i = 0; i < n; i++) {
      x = prompt("Ingresa un número: ", "18");
      nums.push(x);
  }

  ex3(nums);
}

function ex3 (array) {
  var pos = 0, neg = 0, ceros = 0;

  for (var i = 0; i < array.length; i++) {
    if(array[i]<0)
      neg++;
    else if(array[i]>0)
      pos++;
    else
      ceros++;
  }
  window.alert("Hay "+neg+" números negativos, "+ceros+" ceros y "+pos+" números positivos.");
}

//EJERCICIO4
function askMatrix() {
  var n, m, nums = [], x;

  n = prompt("¿Cuántos renglones vas a ingresar?", "4");
  m = prompt("¿Cuántas columnas?", "3");

  for (var i = 0; i < n; i++) {
    nums[i] = new Array(m);
    window.alert("Vamos a llenar el renglón "+(i+1)+"...")
    for (var j = 0; j < m; j++) {
      nums[i][j] = prompt("Ingresa un número: ", "100");
    }
  }

  ex4(nums);
}

function ex4 (matriz) {
  var suma = 0, avg = 0, avgStr = '';
  for(var i=0; i<matriz.length; i++) { //row
    for (var j=0; j < matriz[i].length; j++) { //column
      suma += parseInt(matriz[i][j]);
    }
    avg = (suma/matriz[i].length).toFixed(2);
    avgStr += "Promedio "+(i+1)+": "+avg+"\n";
    suma = avg = 0;
  }
  window.alert(avgStr);
}

//EJERCICIO5
function ex5(num) {
  var res = 0;
  while(num > 0) {
    res = (num%10) + (res*10);
    num = Math.floor(num/10);
  }
  window.alert("El número con sus dígitos invertidos es: "+res);
}

//EJERCICIO6
function workout() {
  nombre = prompt("Workout name: ","Running 28/01/19");
  var div = document.getElementById('workoutName');
  div.innerHTML = nombre;

  document.getElementById("end").style.visibility = "hidden";
  document.getElementById("max").style.visibility = "hidden";
  document.getElementById("min").style.visibility = "hidden";

  tiempos = [];
  previo = 0;
  actual = 0;

  this.beginRun = beginRun;
  this.endRun = endRun;
  this.maxTime = maxTime;
  this.minTime = minTime;
}

function beginRun() {
  previo = new Date();

  document.getElementById("begin").style.visibility = "hidden";
  document.getElementById("end").style.visibility = "visible";
  document.getElementById("max").style.visibility = "hidden";
  document.getElementById("min").style.visibility = "hidden";
}

function endRun() {
  actual = new Date();
  var tiempo = actual-previo;
  tiempos.push(tiempo);
  var div = document.getElementById('currTime');
  div.innerHTML = "You did "+(tiempo/1000)+" secs";

  document.getElementById("begin").style.visibility = "visible";
  document.getElementById("end").style.visibility = "hidden";
  document.getElementById("max").style.visibility = "visible";
  document.getElementById("min").style.visibility = "visible";
}

function maxTime() {
  var max = tiempos[0];
  for(var i=1; i<this.tiempos.length; i++) {
    if(tiempos[i] > max)
      max = tiempos[i];
  }
  var div = document.getElementById('currTime');
  div.innerHTML = "Your worst time is "+(max/1000)+" secs";
}

function minTime() {
  var min = tiempos[0];
  for(var i=1; i<this.tiempos.length; i++) {
    if(tiempos[i] < min)
      min = tiempos[i];
  }
  var div = document.getElementById('currTime');
  div.innerHTML = "Your best time is "+(min/1000)+" secs";
}
