<?php
  include 'partials/_header_forms.html';

  echo table($_POST["num"]);

  function table($num) {
    if($num<1) {
      echo "Sorry, no puedo hacer eso, bro.";
      return;
    }

    else {
      $table = "<div class='row'><div class='col m6 offset-m3'><table class='centered striped'><thead><tr><th>n</th><th>n^2</th><th>n^3</th></tr></thead><tbody>";
      for ($i = 0; $i < $num; $i++) { //row
        $table = $table."<tr>";
        $table = $table."<td>".($i+1)."</td>";
        $table = $table."<td>".(($i+1)*($i+1))."</td>";
        $table = $table."<td>".(($i+1)*($i+1)*($i+1))."</td>";
        $table = $table."</tr>";
      }
      $table = $table."</tbody></table></div></div>";

      return $table;
    }
  }

  include 'partials/_footer_forms.html';
?>
