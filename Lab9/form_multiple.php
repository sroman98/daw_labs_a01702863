<?php
  include 'partials/_header_forms.html';

  require_once("functions.php");

  $str_nums = $_POST["numeros"];
  $arr_nums = array();
  $arr_nums = explode (",", $str_nums);

  echo string_array($arr_nums);
  echo string_list($arr_nums);

  function string_array($arr) {
    $strarr = "";
    foreach ($arr as $x) {
      if($x === end($arr))
        $strarr = $strarr.$x;
      else
        $strarr = $strarr.$x." - ";
    }
    return $strarr;
  }

  function string_list($arr) {
    $s_array = $arr;
    $rs_array = $arr;
    sort($s_array);
    rsort($rs_array);

    $list = "<ol><li>Promedio: ".average($arr)."</li><li>Media: ".median($arr)."</li><li>Menor a mayor: ".string_array($s_array)."</li><li>Mayor a menor: ".string_array($rs_array)."</li></ol>";
    return $list;
  }

  include 'partials/_footer_forms.html';
?>
