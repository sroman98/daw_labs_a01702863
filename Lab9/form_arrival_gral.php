<?php
  include 'partials/_header_forms.html';

  $n = $_POST["n"];
  $str_nums = $_POST["array_alturas"];
  $arr_nums = array();
  $arr_nums = explode (" ", $str_nums);

  echo arrival_of_the_general($n, $arr_nums);

  function arrival_of_the_general($n, $arr_nums) {
    $curr = $arr_nums[0];
    $max = $min = $curr;
    $imax = $imin = 0;
    $res=0;

    for($i=1; $i<$n; $i++) {
      $curr = $arr_nums[$i];
      if($curr > $max) {
        $max = $curr;
        $imax = $i;
      }
      else if($curr <= $min) {
        $min = $curr;
        $imin = $i;
      }
    }
    $res = $imax + ($n-1-$imin);
    if($imax > $imin) {
      $res-=1;
    }

    return "El general tardaría mínimo ".$res." segundos en acomodar a los soldados.";
  }

  include 'partials/_footer_forms.html';
?>
