<?php
  function median($arr) {
    $num_elements = count($arr);
    sort($arr);

    if($num_elements%2 == 0) {
      $median = ($arr[floor($num_elements/2)-1]+$arr[floor($num_elements/2)])/2;
    }
    else {
      $median = $arr[floor($num_elements/2)];
    }
    return $median;
  }

  function average($arr) {
    $num_elements = count($arr);
    $sum = 0;
    foreach($arr as $x) {
      $sum += $x;
    }
    return $sum / $num_elements;
  }
?>
