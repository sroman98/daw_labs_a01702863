¿Qué hace el primer bloque del código (bloque del IF)?
Elimina el procedimiento si ya existe.

¿Para qué sirve la instrucción GO?
Para terminar el procedimiento.

¿Explica que recibe como parámetro este Procedimiento y qué tabla modifica?
Recibe la clave, la descripción, el costo y el impuesto y modifica la tabla
Materiales.


Consultas con parámetros:
¿Qué recibe como parámetro este procedimiento y qué hace?
Recibe una cadena de caracteres y algún número y devuelve los materiales que en
su descripción contengan esa cadena y su costo sea mayor a ese número.


Contesta en tu reporte las siguientes preguntas de reflexión:
¿Qué ventajas tienen el utilizar Stored Procedures en una aplicación cliente-servidor?
La ejecución en php es más sencilla, se hace separación de tareas porque no creas
los queries en php, ahí solo las ejecutas y en tus sqls las creas. Separar por
funcionalidad es bueno.

¿Qué ventajas tiene utilizar SP en un proyecto?
Es más mantenible el código, más fácil de comprender y de debuggear.

Los otros SP que pide los implementé en el proyecto.
