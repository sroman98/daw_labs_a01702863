<?php
    require_once("util.php");

    //Si se ha enviado la forma
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      //Pongo el header porque ese siempre va
      header_html("Lab 11 - formas php");

      //Si no pone nombre o está mal el nombre carga los errores correspondientes
      if (empty($_POST["nombre"])) {
        $nombreErr = "El nombre es obligatorio";
      } else {
        $nombre = test_input($_POST["nombre"]);
        // checar que el nombre tenga solo letras y espacios
        if (!preg_match("/^[a-zA-Z ]*$/",$nombre)) {
          $nombreErr = "Solo se permiten letras y espacios";
        }
      }

      //Si no pone email o está mal el email carga los errores correspondientes
      if (empty($_POST["email"])) {
        $emailErr = "El email es obligatorio";
      } else {
        $email = test_input($_POST["email"]);
        // checar que sea un email válido
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
          $emailErr = "El email no es válido";
        }
      }

      //Si no pone género carga el error
      if (empty($_POST["genero"])) {
        $generoErr = "El género es obligatorio";
      } else {
        $genero = test_input($_POST["genero"]);
      }

      //Si se completó el formulario con éxito se vacía el formulario y se manda un toast con los datos ingresados
      if($nombreErr == "" && $emailErr == "" && $generoErr == "" && !empty($_POST["genero"])) {
        $nombre = $email = $genero = "";

        include("../html/lab11_view.html");
        include("../html/_footer.html");
        //El toast va hasta abajo porque necesita el javascript que cargo en el _footer
        $mensaje = "'¡Se guardó el registro de ".$_POST["nombre"]."!<br>Email: ".$_POST["email"]."<br>Género: ".$_POST["genero"]."'";
        info($mensaje);
      }
      //Si no se completó, igual tiene que mostrar el formulario para mostrar los errores
      else {
        include("../html/lab11_view.html");
        include("../html/_footer.html");
      }
    }

    //Si no se envía el formulario es porque es la primera vez que se abre, inicio las con string vacío y muestro todo normal
    else {
      $nombreErr = $emailErr = $generoErr = "";
      $nombre = $email = $genero = "";

      header_html("Lab 11 - formas php");
      include("../html/lab11_view.html");
      include("../html/_footer.html");
    }

?>
