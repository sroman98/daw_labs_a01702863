<?php

  function header_html($titulo="Lab 11. Formas con php") {
     include("../html/_header.html");
  }

  function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }

  function info($mensaje) {
     $mensaje = '<script> M.toast({html: '.$mensaje.', displayLength: 10000})</script>';
     echo $mensaje;
  }

?>
