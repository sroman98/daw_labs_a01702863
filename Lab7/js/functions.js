//Estas son todas las clases y ids que voy a usar
//Precios
precioIPad11 = document.getElementById('precioIPad11');
precioIPad12 = document.getElementById('precioIPad12');
precioApplePencil = document.getElementById('precioApplePencil');
//Botones
agregarIPad11 = document.getElementById('agregarIPad11');
agregarIPad12 = document.getElementById('agregarIPad12');
agregarApplePencil = document.getElementById('agregarApplePencil');
eliminarIPad11 = document.getElementById('eliminarIPad11');
eliminarIPad12 = document.getElementById('eliminarIPad12');
eliminarApplePencil = document.getElementById('eliminarApplePencil');
comprar = document.getElementById('comprar');
//Cantidades de productos (inputs)
cantidadIPad11 = document.getElementsByClassName('cantidadIPad11');
cantidadIPad12 = document.getElementsByClassName('cantidadIPad12');
cantidadApplePencil = document.getElementsByClassName('cantidadApplePencil');
//tarjetas de productos en carrito
carrito = document.getElementById('carrito');
carritoIPad11 = document.getElementById('carritoIPad11');
carritoIPad12 = document.getElementById('carritoIPad12');
carritoApplePencil = document.getElementById('carritoApplePencil');
//totales
divSubtotal = document.getElementById('subtotal');
subtotal = 0;
divEnvio = document.getElementById('envio');
envio = 0;
divIva = document.getElementById('iva');
iva = 0;
divTotal = document.getElementById('total');
total = 0;


//Ahora sí van las funciones
window.onload = function () {
  //Hago esto para poder checar desde un principio el display de estos elementos
  carritoIPad11.style.display = "none";
  carritoIPad12.style.display = "none";
  carritoApplePencil.style.display = "none";
};

//Para validar las cantidades
cantidadIPad11[0].oninput = function () {verifyQuantity(this);};
cantidadIPad11[1].oninput = function () {verifyQuantity(this,1);};
cantidadIPad12[0].oninput = function () {verifyQuantity(this);};
cantidadIPad12[1].oninput = function () {verifyQuantity(this,1);};
cantidadApplePencil[0].oninput = function () {verifyQuantity(this);};
cantidadApplePencil[1].oninput = function () {verifyQuantity(this,1);};

function verifyQuantity(element, refresh) {
  if(element.value < 1 || element.value > 6) {
    alert("You cannot buy less than 1 or more than 6 items of the same kind.")
    element.value = 1;
  }
  if(refresh)
    actualizarCarrito();
};

//Para agregar al carrito
agregarIPad11.onclick = function () {agregarAlCarrito(this);};
agregarIPad12.onclick = function () {agregarAlCarrito(this);};
agregarApplePencil.onclick = function () {agregarAlCarrito(this);};

function agregarAlCarrito(element) {
  carrito.style.display = "block";
  if(element == agregarIPad11) {
    cantidadIPad11[1].value = cantidadIPad11[0].value;
    carritoIPad11.style.display = "block";
  }
  else if(element == agregarIPad12) {
    cantidadIPad12[1].value = cantidadIPad12[0].value;
    carritoIPad12.style.display = "block";
  }
  else {
    cantidadApplePencil[1].value = cantidadApplePencil[0].value;
    carritoApplePencil.style.display = "block";
  }
  actualizarCarrito();
  window.location.href="#carrito";
};

//Para eliminar del carrito
eliminarIPad11.onclick = function () {eliminarDelCarrito(this);};
eliminarIPad12.onclick = function () {eliminarDelCarrito(this);};
eliminarApplePencil.onclick = function () {eliminarDelCarrito(this);};

function eliminarDelCarrito(element) {
  if(element == eliminarIPad11) {
    cantidadIPad11[1].value = 0;
    carritoIPad11.style.display = "none";
  }
  else if(element == eliminarIPad12) {
    cantidadIPad12[1].value = 0;
    carritoIPad12.style.display = "none";
  }
  else {
    cantidadApplePencil[1].value = 0;
    carritoApplePencil.style.display = "none";
  }
  if(carritoIPad11.style.display == "none" && carritoIPad12.style.display == "none" && carritoApplePencil.style.display == "none") {
    carrito.style.display = "none";
  }
  actualizarCarrito();
};


//Hace el cálculo del carrito
function actualizarCarrito() {
  var precio11 = precioIPad11.textContent.replace("$","").replace(",","");
  var precio12 = precioIPad12.textContent.replace("$","").replace(",","");
  var precioap = precioApplePencil.textContent.replace("$","").replace(",","");
  subtotal = precio11*parseInt(cantidadIPad11[1].value) + precio12*parseInt(cantidadIPad12[1].value) + precioap * parseInt(cantidadApplePencil[1].value);
  if(subtotal > 10000)
    envio = 0;
  else
    envio = 250;
  iva = (0.16*subtotal)/1.16;
  total = subtotal + envio;

  divSubtotal.innerHTML = "$"+formatMoney(subtotal);
  divEnvio.innerHTML = "$"+formatMoney(envio);
  divIva.innerHTML = "$"+formatMoney(iva);
  divTotal.innerHTML = "$"+formatMoney(total);
};

//Me la encontré en stack overflow gg https://stackoverflow.com/questions/149055/how-can-i-format-numbers-as-dollars-currency-string-in-javascript
function formatMoney(n, c, d, t) {
  var c = isNaN(c = Math.abs(c)) ? 2 : c,
    d = d == undefined ? "." : d,
    t = t == undefined ? "," : t,
    s = n < 0 ? "-" : "",
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
    j = (j = i.length) > 3 ? j % 3 : 0;

  return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};


//Cuando por fin le dan click en comprar
  comprar.onclick = function() {
    window.alert("¡Tu compra ha sido exitosa!");
    location.reload();
    window.location.href="#ex2";
  };
