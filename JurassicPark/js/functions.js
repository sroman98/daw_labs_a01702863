$(document).ready(function(){
    //Esconder error
    $('#mensaje').hide();
    
    //Iniciar dropdowns
    $('select').formSelect();
    
    //Submitear forma
    $('#enviar').click(function() {
        // validate and process form here
        $('#mensaje').hide();
        var selectedLugar = $('#lugar').children("option:selected").val();
        var selectedTipo = $('#tipo').children("option:selected").val();
        
        if (selectedLugar == " " | selectedTipo == " ") {
            $('#mensaje').show();
            return false;
        }
        
        // submit form
        //alert (dataString);return false;
        $.post("php/registroIncidente.php", {lugar: selectedLugar, tipo: selectedTipo} )
            
            .done(function( data ) {
               var ajaxResponse = document.getElementById('mensaje');
               ajaxResponse.innerHTML = data;
               ajaxResponse.style.visibility = "visible";
               M.toast({html: 'Incidente Registrado', classes: 'rounded', displayLength:3000});
            }
        );
        return false;
    });
    
    //Actualizar reporte
    window.setInterval(function(){
        $.get("php/tablaIncidentes.php", {accion: 1} )
            .done(function( data ) {
               var ajaxResponse = document.getElementById('bodyIncidentes');
               ajaxResponse.innerHTML = data;
               ajaxResponse.style.visibility = "visible";
            }
        );
        $.get("php/tablaIncidentes.php", {accion: 2} )
            .done(function( data ) {
               var ajaxResponse = document.getElementById('numIncidentes');
               ajaxResponse.innerHTML = data;
               ajaxResponse.style.visibility = "visible";
            }
        );
    }, 2000);
 });
