<?php
    require_once 'util.php';

    if($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['accion'])) {
        $result=getIncidentes();
        $numIncidentes=mysqli_num_rows($result);
        if($_GET['accion']==1) {
            if($numIncidentes > 0) {
                while($row = mysqli_fetch_assoc($result)) {
                    echo "<tr><td>".$row['fechaHora']."</td><td>".$row['nombreLugar']."</td><td>".$row['nombreTipo']."</td></tr>";
                }
            }
            else {
                echo "<tr><td>No has registrado incidentes aún.</td></tr>";
            }
        } else {
            echo "Incidentes(".$numIncidentes.")";
        }
        
    }
    
?>