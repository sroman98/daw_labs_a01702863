<?php
    function connectDB() {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "JurassicPark";
    
        $con = mysqli_connect($servername, $username, $password, $dbname);
    
        if(!$con) {
          die("Connection failed: ".mysqli_connect_error());
        }
    
        return $con;
    }

    function closeDB($con) {
        mysqli_close($con);
    }
    
    function getIncidentes() {
        $con = connectDB();
        
        $query="SELECT * FROM IncidentesSinIDs";
        
        $result = mysqli_query($con, $query);

        closeDB($con);
        
        return $result;
    }
    
    function registrarIncidente($lugar, $tipo) {
        $con = connectDB();
        
        $lugar = $con->real_escape_string($lugar);
        $tipo = $con->real_escape_string($tipo);
        
        echo $lugar;
        echo $tipo;

        $query="CALL insertarIncidente (".$lugar.", ".$tipo.")";
        $results=mysqli_query($con,$query);

        closeDB($con);
    }
    
    function getLugares() {
        
        $con = connectDB();
        
        $query="SELECT idLugar, nombreLugar FROM Lugares";
        
        $result = mysqli_query($con, $query);

        closeDB($con);
        
        return $result;
    }
    
    function printOptionsLugares() {
        $res = getLugares();
        if(mysqli_num_rows($res) > 0) {
            echo "<option value=' ' disabled selected>Selecciona una opción</option>";
            while ($row = mysqli_fetch_assoc($res)) {
                echo "<option value='".$row['idLugar']."'>".$row['nombreLugar']."</option>";
            }
        }
    }
    
    function getTipos() {
        
        $con = connectDB();
        
        $query="SELECT idTipo, nombreTipo FROM Tipos";
        
        $result = mysqli_query($con, $query);

        closeDB($con);
        
        return $result;
    }
    
    function printOptionsTipos() {
        $res = getTipos();
        if(mysqli_num_rows($res) > 0) {
            echo "<option value=' ' disabled selected>Selecciona una opción</option>";
            while ($row = mysqli_fetch_assoc($res)) {
                echo "<option value='".$row['idTipo']."'>".$row['nombreTipo']."</option>";
            }
        }
    }
?>

