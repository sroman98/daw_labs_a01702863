
CREATE TABLE IF NOT EXISTS `Incidentes` (
  `idIncidente` int(10) NOT NULL AUTO_INCREMENT,
  `fechaHora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lugar` int(5) NOT NULL,
  `tipo` int(5) NOT NULL,
  PRIMARY KEY (`idIncidente`),
  KEY `lugar` (`lugar`,`tipo`),
  KEY `lugar_2` (`lugar`),
  KEY `tipo` (`tipo`),
  
  CONSTRAINT `Incidentes_ibfk_2` FOREIGN KEY (`tipo`) REFERENCES `Tipos` (`idTipo`),
  CONSTRAINT `Incidentes_ibfk_1` FOREIGN KEY (`lugar`) REFERENCES `Lugares` (`idLugar`);
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `Lugares` (
  `idLugar` int(5) NOT NULL AUTO_INCREMENT,
  `nombreLugar` varchar(50) NOT NULL,
  PRIMARY KEY (`idLugar`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;


INSERT INTO `Lugares` (`idLugar`, `nombreLugar`) VALUES
(1, 'Centro turístico'),
(2, 'Laboratorios'),
(3, 'Restaurante'),
(4, 'Centro operativo'),
(5, 'Triceratops'),
(6, 'Dilofosaurios'),
(7, 'Velociraptors'),
(8, 'TRex'),
(9, 'Planicie de los herbívoros');


CREATE TABLE IF NOT EXISTS `Tipos` (
  `idTipo` int(5) NOT NULL AUTO_INCREMENT,
  `nombreTipo` varchar(50) NOT NULL,
  PRIMARY KEY (`idTipo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

INSERT INTO `Tipos` (`idTipo`, `nombreTipo`) VALUES
(1, 'Falla eléctrica'),
(2, 'Fuga de herbívoro'),
(3, 'Fuga de Velociraptors'),
(4, 'Fuga de TRex'),
(5, 'Robo de ADN'),
(6, 'Auto descompuesto'),
(7, 'Visitantes en zona no autorizada');


