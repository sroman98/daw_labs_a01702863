CREATE VIEW `IncidentesSinIDs` AS 
select `I`.`fechaHora` AS `fechaHora`,`L`.`nombreLugar` AS `nombreLugar`,`T`.`nombreTipo` AS `nombreTipo` 
from ((`Incidentes` `I` join `Lugares` `L`) join `Tipos` `T`)
where ((`I`.`lugar` = `L`.`idLugar`) and (`I`.`tipo` = `T`.`idTipo`)) 
order by `I`.`fechaHora` desc;