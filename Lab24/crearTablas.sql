IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Clientes_Banca')
drop table Clientes_Banca
CREATE TABLE Clientes_Banca
(
  NoCuenta varchar(5),
  Nombre varchar(30),
  Saldo numeric(10,2),

  constraint llaveClientes_Banca PRIMARY KEY (NoCuenta)
)

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Tipos_Movimiento')
drop table Tipos_Movimiento
CREATE TABLE Tipos_Movimiento
(
  ClaveM varchar(2),
  Descripcion varchar(30),

  constraint llaveTipos_Movimiento PRIMARY KEY (ClaveM)
)

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Movimientos')
drop table Movimientos
CREATE TABLE Movimientos
(
  NoCuenta varchar(5),
  ClaveM varchar(2),
  Fecha datetime,
  Monto numeric(10,2),

  constraint llaveMovimientos PRIMARY KEY (NoCuenta, ClaveM, Fecha),
  constraint fkMovimientosNoCuenta foreign key (NoCuenta) references Clientes_Banca(NoCuenta),
  constraint fkMovimientosClaveM foreign key (ClaveM) references Tipos_Movimiento(ClaveM)
)