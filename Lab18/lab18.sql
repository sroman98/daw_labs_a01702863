/*1) La suma de las cantidades e importe total de todas las entregas realizadas
durante el 97.*/
SET DATEFORMAT dmy
SELECT SUM(e.Cantidad) as 'Total cantidades', SUM(m.Costo*e.Cantidad) as 'Importe total'
FROM Entregan e, Materiales m
WHERE e.Clave=m.Clave AND e.Fecha BETWEEN '01/01/1997' AND '31/12/1997'

/*2) Para cada proveedor, obtener la razón social del proveedor, número de
entregas e importe total de las entregas realizadas.*/
SELECT p.RazonSocial, COUNT(*) as 'Num entregas', SUM(m.Costo*e.Cantidad) as 'Importe total'
FROM Proveedores p, Entregan e, Materiales m
WHERE e.RFC=p.RFC AND e.Clave=m.Clave
GROUP BY p.RazonSocial


/*3) Por cada material obtener la clave y descripción del material, la cantidad
total entregada, la mínima cantidad entregada, la máxima cantidad entregada, el
importe total de las entregas de aquellos materiales en los que la cantidad
promedio entregada sea mayor a 400.*/
SELECT m.Clave, m.Descripcion, SUM(e.Cantidad) AS 'Cant total', MIN(e.Cantidad) AS 'Min Cant', MAX(e.Cantidad) AS 'Max Cant', SUM(m.Costo*e.Cantidad) AS 'Importe total'
FROM Materiales m, Entregan e
WHERE m.Clave=e.Clave
GROUP BY m.Clave, m.Descripcion
HAVING AVG(e.Cantidad) > 400

/*
4) Para cada proveedor, indicar su razón social y mostrar la cantidad promedio
de cada material entregado, detallando la clave y descripción del material,
excluyendo aquellos proveedores para los que la cantidad promedio sea menor a
500.
 */
SELECT p.RazonSocial, m.Clave, m.Descripcion, AVG(e.Cantidad) AS 'CantProm'
FROM Proveedores p, Materiales m, Entregan e
WHERE p.RFC=e.RFC AND m.Clave=e.Clave
GROUP BY p.RazonSocial, m.Clave, m.Descripcion
HAVING AVG(e.Cantidad) >= 500

/*
5) Mostrar en una solo consulta los mismos datos que en la consulta anterior
pero para dos grupos de proveedores: aquellos para los que la cantidad promedio
entregada es menor a 370 y aquellos para los que la cantidad promedio entregada
sea mayor a 450.
 */
SELECT p.RazonSocial, m.Clave, m.Descripcion, AVG(e.Cantidad) AS 'CantProm'
FROM Proveedores p, Materiales m, Entregan e
WHERE p.RFC=e.RFC AND m.Clave=e.Clave
GROUP BY p.RazonSocial, m.Clave, m.Descripcion
HAVING AVG(e.Cantidad) < 370 OR AVG(e.Cantidad) > 450
ORDER BY CantProm

/*
Insertar 5 nuevos materiales
 */
INSERT INTO Materiales VALUES (1440, 'Madera roja', 220, 2.1)
INSERT INTO Materiales VALUES (1450, 'Madera verde', 210, 2.2)
INSERT INTO Materiales VALUES (1460, 'Madera amarilla', 250, 2.0)
INSERT INTO Materiales VALUES (1470, 'Madera café', 215, 2.4)
INSERT INTO Materiales VALUES (1480, 'Madera roble', 230, 2.1)

/*
11) Clave y descripción de los materiales que nunca han sido entregados.
 */
SELECT m.Clave, m.Descripcion
FROM Materiales m
WHERE m.Clave NOT IN (
  SELECT e.Clave
  FROM Entregan e
  )

/*
12) Razón social de los proveedores que han realizado entregas tanto al proyecto
'Vamos México' como al proyecto 'Querétaro Limpio'.
 */
SELECT DISTINCT p.RazonSocial
FROM Proveedores p, Entregan e, Proyectos pr
WHERE p.RFC=e.RFC AND pr.Numero=e.Numero AND pr.Denominacion='Vamos Mexico' AND p.RFC IN (
  SELECT p2.RFC
  FROM Proveedores p2, Entregan e2, Proyectos pr2
  WHERE p2.RFC=e2.RFC AND pr2.Numero=e2.Numero AND pr2.Denominacion='Queretaro Limpio'
  )

/*
13) Descripción de los materiales que nunca han sido entregados al proyecto
'CIT Yucatán'.
 */
SELECT m.Descripcion
FROM Materiales m
WHERE m.Clave NOT IN (
  SELECT e.Clave
  FROM Proyectos p, Entregan e
  WHERE p.Numero=e.Numero AND p.Denominacion='CIT Yucatan'
  )

/*
14) Razón social y promedio de cantidad entregada de los proveedores cuyo
promedio de cantidad entregada es mayor al promedio de la cantidad entregada por
el proveedor con el RFC 'VAGO780901'.
 */
INSERT INTO Proveedores VALUES ('VAGO780901', 'Mi Razon Social')
INSERT INTO Entregan VALUES (1000,'VAGO780901',5000,'1998-07-08 00:00:00.000',165.00)
INSERT INTO Entregan VALUES (1050,'VAGO780901',5002,'1998-07-08 00:00:00.000',700.00)
INSERT INTO Entregan VALUES (1050,'VAGO780901',5002,'1998-07-09 00:00:00.000',1.00)

SELECT p.RazonSocial, AVG(e.Cantidad) AS 'PromCant'
FROM Proveedores p, Entregan e
WHERE p.RFC=e.RFC
GROUP BY p.RazonSocial
HAVING AVG(e.Cantidad) > (
  SELECT AVG(e2.Cantidad)
  FROM Entregan e2
  WHERE e2.RFC='VAGO780901'
  )

/*
15) RFC, razón social de los proveedores que participaron en el proyecto
'Infonavit Durango' y cuyas cantidades totales entregadas en el 2000 fueron
mayores a las cantidades totales entregadas en el 2001.
 */
SET DATEFORMAT dmy
SELECT p.RFC, p.RazonSocial
FROM Proveedores p, Proyectos pr, Entregan e
WHERE p.RFC=e.RFC AND pr.Numero=e.Numero AND pr.Denominacion='Infonavit Durango'
GROUP BY p.RFC, p.RazonSocial
HAVING (
  SELECT SUM(e.Cantidad)
  FROM Entregan e
  WHERE e.Fecha BETWEEN '01/01/2000' AND '31/12/2000' AND e.RFC=p.RFC
         ) > (
  SELECT SUM(e.Cantidad)
  FROM Entregan e
  WHERE e.Fecha BETWEEN '01/01/2001' AND '31/12/2001' AND e.RFC=p.RFC
  )