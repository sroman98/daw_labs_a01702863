<?php
  session_start();
  require_once "util.php";

  include '_header.html';

  if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['idMod'])) {
    if($_GET['idMod']!="") {
      $_GET['idMod'] = htmlspecialchars($_GET['idMod']);

      $_SESSION['idMod'] = $_GET['idMod'];

      $idProd = getProductById($_GET['idMod']);

      if(mysqli_num_rows($idProd) > 0) {
        while($row = mysqli_fetch_assoc($idProd)) {
          $_SESSION['id'] = $row['idProducto'];
          $_SESSION['nombre'] = $row['nombre'];
          $_SESSION['precio'] = $row['precio'];
          $_SESSION['descripcion'] = $row['descripcion'];
          $_SESSION['fechaCaducidad'] = $row['fechaCaducidad'];
          if ($row['disponible']==1) {
            $_SESSION['disponible'] = 'sí';
          }
          else {
            $_SESSION['disponible'] = 'no';
          }
          switch ($row['idCategoria']) {
            case '1':
              $_SESSION['categoria'] = 'comida';
              break;
            case '2':
              $_SESSION['categoria'] = 'bebida';
              break;
            case '3':
              $_SESSION['categoria'] = 'postre';
              break;
            default:
              $_SESSION['categoria'] = 'NA';
              break;
          }

        }
        include 'modificarProd_view.html';
      }
      else {
        echo "<p>¡ERROR!</p>";
        echo "<a class='waves-effect waves-light btn-small indigo lighten-1' href='index.php'>Volver al inicio</a>";
      }
    }
    else {
      echo "<p>¡ERROR!</p>";
      echo "<a class='waves-effect waves-light btn-small indigo lighten-1' href='index.php'>Volver al inicio</a>";
    }
  }
  else if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['search_product'])) {
    $_POST['search_product'] = htmlspecialchars($_POST['search_product']);
    $result = getProductsByName($_POST['search_product']);

    //Esto lo hago igual abajo en el else, podría ser una función
    if(mysqli_num_rows($result) > 0) {
      $table1 = "";
      while($row = mysqli_fetch_assoc($result)) {
        $table1.= "<tr>";
        $table1.= "<td><a href='modificar.php?idMod=".$row['idProducto']."'>".$row['idProducto']."</a></td>";
        $table1.= "<td>".$row['nombre']."</td>";
        $table1.= "<td>$".$row['precio']."</td>";
        $table1.= "<td>".$row['descripcion']."</td>";
        $table1.= "<td>".$row['fechaCaducidad']."</td>";
        if ($row['disponible']==1) {
          $table1.= "<td>sí</td>";
        }
        else {
          $table1.= "<td>no</td>";
        }
        switch ($row['idCategoria']) {
          case '1':
            $table1.= "<td>comida</td>";
            break;
          case '2':
            $table1.= "<td>bebida</td>";
            break;
          case '3':
            $table1.= "<td>postre</td>";
            break;
          default:
            $table1.= "<td>NA</td>";
            break;
        }
        $table1.= "</tr>";
      }
    }
    else {
      $table1 = "";
    }
    // Final de esto

    include 'modificar_view.html';
  }
  else if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['nombre'])) {
    if(isset($_POST['precio']) && isset($_POST['descripcion']) && isset($_POST['categoria']) && isset($_POST['disponible'])) {
      $_POST['nombre'] = htmlspecialchars($_POST['nombre']);
      if($_POST['nombre']=="") {
        $_POST['nombre'] = $_SESSION['nombre'];
      }
      $_POST['precio'] = htmlspecialchars($_POST['precio']);
      if($_POST['precio']=="") {
        $_POST['precio'] = $_SESSION['precio'];
      }
      $_POST['descripcion'] = htmlspecialchars($_POST['descripcion']);
      if($_POST['descripcion']=="") {
        $_POST['descripcion'] = $_SESSION['descripcion'];
      }
      $_POST['caducidad'] = htmlspecialchars($_POST['caducidad']);
      if($_POST['caducidad']=="") {
        $_POST['caducidad'] = $_SESSION['caducidad'];
      }
      $_POST['disponible'] = htmlspecialchars($_POST['disponible']);
      $_POST['categoria'] = htmlspecialchars($_POST['categoria']);

      modifyProduct($_SESSION['idMod'], $_POST['nombre'], $_POST['precio'], $_POST['descripcion'], $_POST['caducidad'], $_POST['disponible'], $_POST['categoria']);

      echo "<p class ='green lighten-3'>Se ha modificado el producto con éxito.</p>";
      echo "<div class='row'>";
      echo "<div class='col m4'><a class='waves-effect waves-light btn-small indigo lighten-1' href='modificar.php'>Modificar otro producto</a></div>";
      echo "<div class='col m4'><a class='waves-effect waves-light btn-small indigo lighten-1' href='index.php'>Volver al inicio</a></div>";
      echo "</div>";
    }
    else {
      echo "<p>¡ERROR!</p>";
      echo "<a class='waves-effect waves-light btn-small indigo lighten-1' href='index.php'>Volver al inicio</a>";
    }
  }
  else {
    $result = getProducts();
    if(mysqli_num_rows($result) > 0) {
      $table1 = "";
      while($row = mysqli_fetch_assoc($result)) {
        $table1.= "<tr>";
        $table1.= "<td><a href='modificar.php?idMod=".$row['idProducto']."'>".$row['idProducto']."</a></td>";
        $table1.= "<td>".$row['nombre']."</td>";
        $table1.= "<td>$".$row['precio']."</td>";
        $table1.= "<td>".$row['descripcion']."</td>";
        $table1.= "<td>".$row['fechaCaducidad']."</td>";
        if ($row['disponible']==1) {
          $table1.= "<td>sí</td>";
        }
        else {
          $table1.= "<td>no</td>";
        }
        switch ($row['idCategoria']) {
          case '1':
            $table1.= "<td>comida</td>";
            break;
          case '2':
            $table1.= "<td>bebida</td>";
            break;
          case '3':
            $table1.= "<td>postre</td>";
            break;
          default:
            $table1.= "<td>NA</td>";
            break;
        }
        $table1.= "</tr>";
      }
    }
    else {
      $table1 = "";
    }
    include 'modificar_view.html';
  }
    include '_footer.html';

?>
