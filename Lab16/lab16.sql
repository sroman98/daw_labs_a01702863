-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Mar 12, 2019 at 04:54 AM
-- Server version: 5.7.25
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `lab14`
--

-- --------------------------------------------------------

--
-- Table structure for table `Productos`
--

CREATE TABLE `Productos` (
  `idProducto` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `precio` float NOT NULL,
  `descripcion` varchar(60) NOT NULL,
  `fechaCaducidad` date DEFAULT NULL,
  `disponible` tinyint(1) NOT NULL,
  `idCategoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Productos`
--

INSERT INTO `Productos` (`idProducto`, `nombre`, `precio`, `descripcion`, `fechaCaducidad`, `disponible`, `idCategoria`) VALUES
(11, 'Gansitos', 10, 'Producto de chocolate Marinela relleno de fresa y crema', NULL, 1, 3),
(12, 'Pingüinos', 13.5, 'Producto de chocolate Marinela relleno de chocolate', NULL, 1, 3),
(14, 'Runners', 12.5, 'Frituras de marca Barcel en forma de coche', NULL, 1, 1),
(15, 'Molletes', 25, 'Bolillo con frijoles y queso', NULL, 1, 1),
(16, 'Chilaquiles', 15, 'Totopos bañados en salsa verde con pollo queso y crema', NULL, 0, 1),
(17, 'Café Americano', 10, 'Café de tipo americano', NULL, 1, 2),
(19, 'Café Expresso', 6, 'Shot de café', NULL, 0, 2),
(20, 'Jugo de naranja', 10, 'Jugo de naranja fresco', NULL, 1, 2),
(21, 'Pizza', 25, 'Pizza de peperoni', NULL, 1, 1),
(22, 'Cheetos Puff', 7, 'Cheetos azules', '2020-08-21', 1, 1),
(23, 'Magnum', 27, 'Paleta de helado de vainilla cubierta de chocolate', '2019-03-18', 1, 3),
(24, 'Coca-Cola', 7, 'Refresco de cola', '2019-11-21', 1, 2),
(25, 'Yakult', 7, 'Bebida deliciosa', '2019-03-16', 0, 2),
(26, 'Gorditas', 16, 'Gorditas de migajas', NULL, 0, 1),
(27, 'Helado', 13, 'Helado sabor vainilla', '2019-10-14', 1, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Productos`
--
ALTER TABLE `Productos`
  ADD PRIMARY KEY (`idProducto`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Productos`
--
ALTER TABLE `Productos`
  MODIFY `idProducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
