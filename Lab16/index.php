<?php
  session_start();
  require_once "util.php";

  include '_header.html';
  include 'index_view.html';
  include '_footer.html';

  if(isset($_GET['func'])) {
    header("location: ".$_GET['func'].".php");
  }

?>
