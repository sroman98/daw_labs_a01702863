<?php
  session_start();
  require_once "util.php";

  include '_header.html';

  if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['search_product'])) {
    $_POST['search_product'] = htmlspecialchars($_POST['search_product']);
    $result = getProductsByName($_POST['search_product']);

    //Esto lo hago igual abajo en el else, podría ser una función
    if(mysqli_num_rows($result) > 0) {
      $table1 = "";
      while($row = mysqli_fetch_assoc($result)) {
        $table1.= "<tr>";
        $table1.= "<td>".$row['idProducto']."</td>";
        $table1.= "<td>".$row['nombre']."</td>";
        $table1.= "<td>$".$row['precio']."</td>";
        $table1.= "<td>".$row['descripcion']."</td>";
        $table1.= "<td>".$row['fechaCaducidad']."</td>";
        if ($row['disponible']==1) {
          $table1.= "<td>sí</td>";
        }
        else {
          $table1.= "<td>no</td>";
        }
        switch ($row['idCategoria']) {
          case '1':
            $table1.= "<td>comida</td>";
            break;
          case '2':
            $table1.= "<td>bebida</td>";
            break;
          case '3':
            $table1.= "<td>postre</td>";
            break;
          default:
            $table1.= "<td>NA</td>";
            break;
        }
        $table1.= "</tr>";
      }
    }
    else {
      $table1 = "";
    }
    // Final de esto

    include 'ver_view.html';
  }
  else {
    $result = getProducts();
    if(mysqli_num_rows($result) > 0) {
      $table1 = "";
      while($row = mysqli_fetch_assoc($result)) {
        $table1.= "<tr>";
        $table1.= "<td>".$row['idProducto']."</td>";
        $table1.= "<td>".$row['nombre']."</td>";
        $table1.= "<td>$".$row['precio']."</td>";
        $table1.= "<td>".$row['descripcion']."</td>";
        $table1.= "<td>".$row['fechaCaducidad']."</td>";
        if ($row['disponible']==1) {
          $table1.= "<td>sí</td>";
        }
        else {
          $table1.= "<td>no</td>";
        }
        switch ($row['idCategoria']) {
          case '1':
            $table1.= "<td>comida</td>";
            break;
          case '2':
            $table1.= "<td>bebida</td>";
            break;
          case '3':
            $table1.= "<td>postre</td>";
            break;
          default:
            $table1.= "<td>NA</td>";
            break;
        }
        $table1.= "</tr>";
      }
    }
    else {
      $table1 = "";
    }
    include 'ver_view.html';
  }
    include '_footer.html';

?>
