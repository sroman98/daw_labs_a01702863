<?php
  session_start();
  require_once "util.php";

  include '_header.html';

  if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['idMod']) && !isset($_GET['eliminar'])) {
    if($_GET['idMod']!="") {
      $_GET['idMod'] = htmlspecialchars($_GET['idMod']);

      $_SESSION['idMod'] = $_GET['idMod'];

      $idProd = getProductById($_GET['idMod']);

      if(mysqli_num_rows($idProd) > 0) {
        while($row = mysqli_fetch_assoc($idProd)) {
          $_SESSION['id'] = $row['idProducto'];
          $_SESSION['nombre'] = $row['nombre'];
          $_SESSION['precio'] = $row['precio'];
          $_SESSION['descripcion'] = $row['descripcion'];
          $_SESSION['fechaCaducidad'] = $row['fechaCaducidad'];
          if ($row['disponible']==1) {
            $_SESSION['disponible'] = 'sí';
          }
          else {
            $_SESSION['disponible'] = 'no';
          }
          switch ($row['idCategoria']) {
            case '1':
              $_SESSION['categoria'] = 'comida';
              break;
            case '2':
              $_SESSION['categoria'] = 'bebida';
              break;
            case '3':
              $_SESSION['categoria'] = 'postre';
              break;
            default:
              $_SESSION['categoria'] = 'NA';
              break;
          }

        }
        include 'eliminarProd_view.html';
      }
      else {
        echo "<p>¡ERROR!</p>";
        echo "<a class='waves-effect waves-light btn-small indigo lighten-1' href='index.php'>Volver al inicio</a>";
      }
    }
    else {
      echo "<p>¡ERROR!</p>";
      echo "<a class='waves-effect waves-light btn-small indigo lighten-1' href='index.php'>Volver al inicio</a>";
    }
  }
  else if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['search_product'])) {
    $_POST['search_product'] = htmlspecialchars($_POST['search_product']);
    $result = getProductsByName($_POST['search_product']);

    //Esto lo hago igual abajo en el else, podría ser una función
    if(mysqli_num_rows($result) > 0) {
      $table1 = "";
      while($row = mysqli_fetch_assoc($result)) {
        $table1.= "<tr>";
        $table1.= "<td><a href='eliminar.php?idMod=".$row['idProducto']."'>".$row['idProducto']."</a></td>";
        $table1.= "<td>".$row['nombre']."</td>";
        $table1.= "<td>$".$row['precio']."</td>";
        $table1.= "<td>".$row['descripcion']."</td>";
        $table1.= "<td>".$row['fechaCaducidad']."</td>";
        if ($row['disponible']==1) {
          $table1.= "<td>sí</td>";
        }
        else {
          $table1.= "<td>no</td>";
        }
        switch ($row['idCategoria']) {
          case '1':
            $table1.= "<td>comida</td>";
            break;
          case '2':
            $table1.= "<td>bebida</td>";
            break;
          case '3':
            $table1.= "<td>postre</td>";
            break;
          default:
            $table1.= "<td>NA</td>";
            break;
        }
        $table1.= "</tr>";
      }
    }
    else {
      $table1 = "";
    }
    // Final de esto

    include 'eliminar_view.html';
  }
  else if($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['eliminar'])) {
    if($_GET['eliminar']=="si") {
      deleteProduct($_SESSION['idMod']);

      echo "<p class ='green lighten-3'>Se ha eliminado el producto con éxito.</p>";
      echo "<div class='row'>";
      echo "<div class='col m4'><a class='waves-effect waves-light btn-small indigo lighten-1' href='eliminar.php'>Eliminar otro producto</a></div>";
      echo "<div class='col m4'><a class='waves-effect waves-light btn-small indigo lighten-1' href='index.php'>Volver al inicio</a></div>";
      echo "</div>";
    }
    else {
      echo "<p>¡ERROR!</p>";
      echo "<a class='waves-effect waves-light btn-small indigo lighten-1' href='index.php'>Volver al inicio</a>";
    }
  }
  else {
    $result = getProducts();
    if(mysqli_num_rows($result) > 0) {
      $table1 = "";
      while($row = mysqli_fetch_assoc($result)) {
        $table1.= "<tr>";
        $table1.= "<td><a href='eliminar.php?idMod=".$row['idProducto']."'>".$row['idProducto']."</a></td>";
        $table1.= "<td>".$row['nombre']."</td>";
        $table1.= "<td>$".$row['precio']."</td>";
        $table1.= "<td>".$row['descripcion']."</td>";
        $table1.= "<td>".$row['fechaCaducidad']."</td>";
        if ($row['disponible']==1) {
          $table1.= "<td>sí</td>";
        }
        else {
          $table1.= "<td>no</td>";
        }
        switch ($row['idCategoria']) {
          case '1':
            $table1.= "<td>comida</td>";
            break;
          case '2':
            $table1.= "<td>bebida</td>";
            break;
          case '3':
            $table1.= "<td>postre</td>";
            break;
          default:
            $table1.= "<td>NA</td>";
            break;
        }
        $table1.= "</tr>";
      }
    }
    else {
      $table1 = "";
    }
    include 'eliminar_view.html';
  }
    include '_footer.html';

?>
