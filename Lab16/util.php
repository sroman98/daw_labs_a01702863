<?php
  function connectDB() {
    $servername = "localhost";
    $username = "sroman98";
    $password = "s123";
    $dbname = "lab14";

    $con = mysqli_connect($servername, $username, $password, $dbname);

    // Check fann_get_total_connection
    if(!$con) {
      die("Connection failed: ".mysqli_connect_error());
    }

    return $con;
  }

  function closeDB($con) {
    mysqli_close($con);
  }

  function getProducts() {
    $con = connectDB();

    $sql = "SELECT * FROM Productos";

    $result = mysqli_query($con, $sql);

    closeDB($con);

    return $result;
  }

  function getProductsByName($producto) {
    $con = connectDB();

    $sql = "SELECT * FROM Productos WHERE nombre LIKE '%".$producto."%'";

    $result = mysqli_query($con, $sql);

    closeDB($con);

    return $result;
  }

  function getProductById($id) {
    $con = connectDB();

    $sql = "SELECT * FROM Productos WHERE idProducto =".$id."";

    $result = mysqli_query($con, $sql);

    closeDB($con);

    return $result;
  }

  function getAvailableProducts($producto) {
    $con = connectDB();

    $sql = "SELECT idProducto, nombre FROM Productos WHERE disponible=1 AND nombre LIKE '%".$producto."%'";

    $result = mysqli_query($con, $sql);

    closeDB($con);

    return $result;
  }

  function getCheapestProducts($precio) {
    $con = connectDB();

    $sql = "SELECT idProducto, nombre, precio FROM Productos WHERE precio<=".$precio." ORDER BY precio";

    $result = mysqli_query($con, $sql);

    closeDB($con);

    return $result;
  }

  function insertProduct($nombre, $precio, $descripcion, $fechaCaducidad, $disponible, $categoria) {
    $con = connectDB();

    $query='INSERT INTO Productos (idProducto, nombre, precio, descripcion, fechaCaducidad, disponible, idCategoria) VALUES (?,?,?,?,?,?,?)';

    if (!($statement = $con->prepare($query))) {
      die("Preparation failed: (" . $con->errno . ") " . $con->error);
    }

    $id = null;
    $nombre = $con->real_escape_string($nombre);
    $precio = $con->real_escape_string($precio);
    $descripcion = $con->real_escape_string($descripcion);
    if($fechaCaducidad=="") {
      $fechaCaducidad = null;
    }
    else {
        $fechaCaducidad = $con->real_escape_string($fechaCaducidad);
    }
    $disponible = $con->real_escape_string($disponible);
    $categoria = $con->real_escape_string($categoria);

    if (!$statement->bind_param("sssssss", $id, $nombre, $precio, $descripcion, $fechaCaducidad, $disponible, $categoria)) {
      die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
    }

    if (!$statement->execute()) {
      die("Execution failed: (" . $statement->errno . ") " . $statement->error);
    }

    $statement->close();
    closeDB($con);
  }

  function modifyProduct($idProducto, $nombre, $precio, $descripcion, $fechaCaducidad, $disponible, $categoria) {
    $con = connectDB();

    $query='UPDATE Productos SET nombre=?, precio=?, descripcion=?, fechaCaducidad=?, disponible=?, idCategoria=? WHERE idProducto=?';

    if (!($statement = $con->prepare($query))) {
      die("Preparation failed: (" . $con->errno . ") " . $con->error);
    }

    $idProducto = $con->real_escape_string($idProducto);;
    $nombre = $con->real_escape_string($nombre);
    $precio = $con->real_escape_string($precio);
    $descripcion = $con->real_escape_string($descripcion);
    if($fechaCaducidad=="") {
      $fechaCaducidad = null;
    }
    else {
        $fechaCaducidad = $con->real_escape_string($fechaCaducidad);
    }
    $disponible = $con->real_escape_string($disponible);
    $categoria = $con->real_escape_string($categoria);

    if (!$statement->bind_param("sssssss", $nombre, $precio, $descripcion, $fechaCaducidad, $disponible, $categoria, $idProducto)) {
      die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
    }

    if (!$statement->execute()) {
      die("Update failed: (" . $statement->errno . ") " . $statement->error);
    }

    $statement->close();
    closeDB($con);
  }

  function deleteProduct($idProducto) {
    $con = connectDB();

    $query='DELETE FROM Productos WHERE idProducto=?';

    if (!($statement = $con->prepare($query))) {
      die("Preparation failed: (" . $con->errno . ") " . $con->error);
    }

    $idProducto = $con->real_escape_string($idProducto);;

    if (!$statement->bind_param("s", $idProducto)) {
      die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
    }

    if (!$statement->execute()) {
      die("Deletion failed: (" . $statement->errno . ") " . $statement->error);
    }

    $statement->close();
    closeDB($con);
  }
?>
