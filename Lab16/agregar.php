<?php
  session_start();
  require_once "util.php";

  include '_header.html';
  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(isset($_POST['nombre']) && isset($_POST['precio']) && isset($_POST['descripcion']) && isset($_POST['categoria']) && isset($_POST['disponible'])) {

      $_POST['nombre'] = htmlspecialchars($_POST['nombre']);
      $_POST['precio'] = htmlspecialchars($_POST['precio']);
      $_POST['descripcion'] = htmlspecialchars($_POST['descripcion']);
      if(!isset($_POST['caducidad'])) {
        $_POST['caducidad'] = NULL;
      }
      $_POST['caducidad'] = htmlspecialchars($_POST['caducidad']);
      $_POST['disponible'] = htmlspecialchars($_POST['disponible']);
      $_POST['categoria'] = htmlspecialchars($_POST['categoria']);

      insertProduct($_POST['nombre'], $_POST['precio'], $_POST['descripcion'], $_POST['caducidad'], $_POST['disponible'], $_POST['categoria']);
      echo "<p class ='green lighten-3'>Se ha agregado el producto con éxito.</p>";
      echo "<div class='row'>";
      echo "<div class='col m4'><a class='waves-effect waves-light btn-small indigo lighten-1' href='agregar.php'>Agregar otro producto</a></div>";
      echo "<div class='col m4'><a class='waves-effect waves-light btn-small indigo lighten-1' href='index.php'>Volver al inicio</a></div>";
      echo "</div>";
      include '_footer.html';
    }
    else {
      include 'agregar_view.html';
      include '_footer.html';
    }
  }
  else {
    include 'agregar_view.html';
    include '_footer.html';
  }

?>
