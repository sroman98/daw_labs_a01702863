document.addEventListener('DOMContentLoaded', function() {
  var dpoptions = {
        format: 'yyyy-mm-dd'
  };

  //datepicker
  var datepickers = document.querySelectorAll('.datepicker');
  var instances = M.Datepicker.init(datepickers, dpoptions);

  //select
  var selects = document.querySelectorAll('select');
  var instances = M.FormSelect.init(selects);
});
