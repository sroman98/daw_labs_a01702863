BULK INSERT a1702863.a1702863.[Materiales]
   FROM 'e:\wwwroot\a1702863\materiales.csv'
   WITH
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )
