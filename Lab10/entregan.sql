SET DATEFORMAT dmy

BULK INSERT a1702863.a1702863.[Entregan]
   FROM 'e:\wwwroot\a1702863\entregan.csv'
   WITH
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )