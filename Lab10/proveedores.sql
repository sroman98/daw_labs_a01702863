BULK INSERT a1702863.a1702863.[Proveedores]
   FROM 'e:\wwwroot\a1702863\proveedores.csv'
   WITH
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )