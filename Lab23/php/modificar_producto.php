<?php
  session_start();
  require_once("util.php");

  if(isset($_SESSION["idUsuario"]) && allowed(35)) {
    $id=strtolower($_GET['id']);
    $query=getProductoUnico($id);
    $row=mysqli_fetch_assoc($query);
    $nombre=$row["nombre"];
    $descripcion=$row["descripcion"];
    $precio=$row["precio"];
    $disponibilidad=$row["disponible"];
    $categoria=$row["idCategoria"];
    include 'partials/_header.html';
    include "partials/modificar_producto.html";
    include "partials/_footer.html";
  } else {
    header("location: ../index.php");
  }

?>
