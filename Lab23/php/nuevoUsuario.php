<?php
	session_start();
	require_once("util.php");

	include 'partials/_header.html';

	if($_SERVER['REQUEST_METHOD'] == 'POST') {
		//If all data is present
		if (isset($_POST['name']) && isset($_POST['lastname']) && isset($_POST['phone']) && isset($_POST['email']) && isset($_POST['fechanacimiento']) && isset($_POST['passwords']) && isset($_POST['passwordss'])){
		  //Retrieving data by POST, on a safe way, preventing XSS
		  $name = htmlspecialchars($_POST['name'], ENT_QUOTES, 'UTF-8');
		  $lastname = htmlspecialchars($_POST['lastname'], ENT_QUOTES, 'UTF-8');
		  $lastname2 = htmlspecialchars($_POST['lastname2'], ENT_QUOTES, 'UTF-8');
		  $phone = htmlspecialchars($_POST['phone'], ENT_QUOTES, 'UTF-8');
		  $email = htmlspecialchars($_POST['email'], ENT_QUOTES, 'UTF-8');
		  $fechanacimiento = $_POST['fechanacimiento'];
		  $passwords = htmlspecialchars($_POST['passwords'], ENT_QUOTES, 'UTF-8');
		  $passwordss = htmlspecialchars($_POST['passwordss'], ENT_QUOTES, 'UTF-8');
		  $passwordCifrado = password_hash($passwords, PASSWORD_DEFAULT);
		  $ans = createUser($name, $lastname, $lastname2, $phone, $email, $fechanacimiento, $passwordCifrado);
		  if($ans == "exito") {
				include 'partials/nuevoUsuario.html';
				footerhtml();
				echo "<script>M.toast({html: '¡Usuario agregado correctamente!', classes: 'rounded', displayLength:4000});</script>";
			}
			else if($ans == "correo") {
				echo "<div class='row'><div class='col m5 offset-m2 red lighten-1'><p class='center'>¡El correo que ingresaste no existe!</p></div></div>";
				include 'partials/nuevoUsuario.html';
				footerhtml();
			}
			else {
				"<div class='row'><div class='col m5 offset-m2 red lighten-1'><p class='center'>No se pudo registrar el usuario, intenta nuevamente.</p></div></div>";
				include 'partials/nuevoUsuario.html';
				footerhtml();
			}
		} else {
		  echo '<script type="text/javascript">alert("Datos recibidos incorrectamente!");</script>';
		}
	}
	else {
		include 'partials/nuevoUsuario.html';
		footerhtml();
	}


?>
