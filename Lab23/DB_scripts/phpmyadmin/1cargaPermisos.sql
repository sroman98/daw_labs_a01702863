-- Dumping data for table `Permisos`
--

INSERT INTO `Permisos` (`idPermisos`, `permiso`) VALUES
(33, 'verSaldos'),
(34, 'verMiSaldo'),
(35, 'editarProductos'),
(36, 'verProductosDisponibles'),
(37, 'verComprasPendientes'),
(38, 'verMisCompras'),
(39, 'registrarCompraEnLinea'),
(40, 'registrarPagoEnLinea'),
(41, 'editarSaldos'),
(42, 'verPagos'),
(43, 'realizarCorte'),
(44, 'editarTicketCompra'),
(45, 'generarReportes'),
(47, 'editarUsuarios'),
(48, 'editarMiUsuario'),
(49, 'verProductos'),
(50, 'registrarCompraEnPV'),
(51, 'registrarPagoEnPV'),
(53, 'editarMenu');
