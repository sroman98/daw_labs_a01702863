

INSERT INTO `Productos` (`idProducto`, `idCategoria`, `precio`, `descripcion`, `nombre`, `fechaCaducidad`, `disponible`) VALUES
(1, 1, 15, 'Bolillo con frijoles y queso', 'Molletes', 1),
(2, 1, 10, 'Totopos con salsa verde, crema y queso', 'Chilaquiles', 0),
(3, 2, 8, 'Taza de café estilo americano', 'Americano', 1),
(4, 2, 10, 'Taza de café con leche', 'Capuccino', 0),
(5, 2, 8, 'Jugo natural sabor naranja', 'Jugo de naranja', 1),
(6, 3, 7, 'Delicioso producto Marinela ', 'Gansito', 1),
(7, 3, 18, 'Paleta de hielo sabor mango', 'Solero Mango', 0);
