-- Volcado de datos para la tabla `Usuarios`
--

INSERT INTO `Usuarios` (`idUsuario`, `idRol`, `nombre`, `apellidoPaterno`, `apellidoMaterno`, `telefono`, `correo`, `fechaNacimiento`, `contrasena`, `deuda`) VALUES
(NULL, 2, 'Alejandro', 'Gleason', 'Mendoza', '4424675795', 'sara.rodriguez98@gmail.com', '2019-03-03', '$2y$10$Q7lVK/QmxKHrjBoe8VBjEuU/YETDGnHFKLIdwBgzQ84b030MrveFq', 0),
(NULL, 2, 'Salvador', 'Espinosa', 'Guerra', '4421091582', 'salvador.espinosa@imoiap.edu.mx', '2019-03-12', '$2y$10$JDmZNVzEgnJYnimWq2NwGODg9d5ZynODjMOVcYEQ.ovrlO2mHs79W', 0),
(NULL, 1, 'Carlos', 'García', 'Herrera', '4421817718', 'carlos.garcia@imoiap.edu.mx', '2019-03-03', '$2y$10$WxKqfp60ouNdrC2XQ/N00uByjs2TqX/GPHvnqG0o/ODmhhqzKUVHS', 0),
(NULL, 3, 'Guadalupe', 'Contreras', 'Hernández', '4421091589', 'gdlp.contreras@imoiap.edu.mx', '2019-03-01', '$2y$10$sKloQVFjdazESlDlH1yEOereegmtHDYcTT/4sNmt.tMhWNERYo2lO', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
