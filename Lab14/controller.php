<?php
  require_once "util.php";

  include '_header.html';

  $result = getProducts();

  if(mysqli_num_rows($result) > 0) {
    $table1 = "<table class='striped centered'>";
    $table1.= "<thead>";
    $table1.= "<tr><th>ID</th> <th>Nombre</th> <th>Precio</th> <th>Disponible</th></tr>";
    $table1.= "</thead>";
    $table1.= "<tbody>";
    while($row = mysqli_fetch_assoc($result)) {
      $table1.= "<tr>";
      $table1.= "<td>".$row['idProducto']."</td>";
      $table1.= "<td>".$row['nombre']."</td>";
      $table1.= "<td>$".$row['precio']."</td>";
      if ($row['disponible']==1) {
        $table1.= "<td>sí</td>";
      }
      else {
        $table1.= "<td>no</td>";
      }
    }
    $table1.= "</tbody>";
    $table1.= "</table>";
  }
  else {
    $table1 = "No hay productos en el inventario.";
  }


  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $_POST['precio'] = htmlspecialchars($_POST['precio']);
    $_POST['producto'] = htmlspecialchars($_POST['producto']);

    if(isset($_POST['producto']) && $_POST['producto']!="") {
      $result = getAvailableProducts($_POST['producto']);

      if(mysqli_num_rows($result) > 0) {
        $table2 = "<table class='striped centered'>";
        $table2.= "<thead>";
        $table2.= "<tr><th>ID</th> <th>Nombre</th></tr>";
        $table2.= "</thead>";
        $table2.= "<tbody>";
        while($row = mysqli_fetch_assoc($result)) {
          $table2.= "<tr>";
          $table2.= "<td>".$row['idProducto']."</td>";
          $table2.= "<td>".$row['nombre']."</td>";
        }
        $table2.= "</tbody>";
        $table2.= "</table>";
      }
      else {
        $table2 = "No hay productos disponibles con el nombre ".$_POST['producto']." en el inventario.";
      }
    }

    if(isset($_POST['precio']) && $_POST['precio']!="") {
      $result = getCheapestProducts($_POST['precio']);

      if(mysqli_num_rows($result) > 0) {
        $table3 = "<table class='striped centered'>";
        $table3.= "<thead>";
        $table3.= "<tr><th>ID</th> <th>Nombre</th> <th>Precio</th></tr>";
        $table3.= "</thead>";
        $table3.= "<tbody>";
        while($row = mysqli_fetch_assoc($result)) {
          $table3.= "<tr>";
          $table3.= "<td>".$row['idProducto']."</td>";
          $table3.= "<td>".$row['nombre']."</td>";
          $table3.= "<td>$".$row['precio']."</td>";
        }
        $table3.= "</tbody>";
        $table3.= "</table>";
      }
      else {
        $table3 = "No hay productos de menos de $".$_POST['precio']." en el inventario.";
      }
    }
  }

  include 'view.html';
  include '_footer.html';

?>
