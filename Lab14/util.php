<?php
  function connectDB() {
    $servername = "localhost";
    $username = "sroman98";
    $password = "s123";
    $dbname = "lab14";

    $con = mysqli_connect($servername, $username, $password, $dbname);

    // Check fann_get_total_connection
    if(!$con) {
      die("Connection failed: ".mysqli_connect_error());
    }

    return $con;
  }

  function closeDB($con) {
    mysqli_close($con);
  }

  function getProducts() {
    $con = connectDB();

    $sql = "SELECT idProducto, nombre, precio, disponible FROM Productos";

    $result = mysqli_query($con, $sql);

    closeDB($con);

    return $result;
  }

  function getAvailableProducts($producto) {
    $con = connectDB();

    $sql = "SELECT idProducto, nombre FROM Productos WHERE disponible=1 AND nombre LIKE '%".$producto."%'";

    $result = mysqli_query($con, $sql);

    closeDB($con);

    return $result;
  }

  function getCheapestProducts($precio) {
    $con = connectDB();

    $sql = "SELECT idProducto, nombre, precio FROM Productos WHERE precio<=".$precio." ORDER BY precio";

    $result = mysqli_query($con, $sql);

    closeDB($con);

    return $result;
  }
?>
