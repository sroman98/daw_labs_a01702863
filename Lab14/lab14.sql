-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Mar 08, 2019 at 08:45 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `lab14`
--

-- --------------------------------------------------------

--
-- Table structure for table `Fruit`
--

CREATE TABLE `Fruit` (
  `name` varchar(25) NOT NULL,
  `units` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` float NOT NULL,
  `country` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Fruit`
--

INSERT INTO `Fruit` (`name`, `units`, `quantity`, `price`, `country`) VALUES
('fresa', 30, 13, 7.5, 'Holanda'),
('mango', 15, 2, 15, 'Mexico'),
('plátano', 24, 6, 12.5, 'India');

-- --------------------------------------------------------

--
-- Table structure for table `Productos`
--

CREATE TABLE `Productos` (
  `idProducto` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `precio` float NOT NULL,
  `descripcion` varchar(60) NOT NULL,
  `fechaCaducidad` date NOT NULL,
  `disponible` tinyint(1) NOT NULL,
  `idCategoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Productos`
--

INSERT INTO `Productos` (`idProducto`, `nombre`, `precio`, `descripcion`, `fechaCaducidad`, `disponible`, `idCategoria`) VALUES
(11, 'Gansitos', 10, 'Producto de chocolate Marinela relleno de fresa y crema', '2019-12-19', 1, 1),
(12, 'Pingüinos', 13.5, 'Producto de chocolate Marinela relleno de chocolate', '2020-03-27', 1, 1),
(13, 'Sabritas Originales', 12, 'Papas de marca Sabritas de sal bolsa color amarillo', '2020-06-19', 0, 2),
(14, 'Runners', 12.5, 'Frituras de marca Barcel en forma de coche', '2020-05-09', 1, 2),
(15, 'Molletes', 25, 'Bolillo con frijoles y queso', '2019-03-07', 1, 3),
(16, 'Chilaquiles', 15, 'Totopos bañados en salsa verde con pollo queso y crema', '2019-03-07', 0, 3),
(17, 'Café Americano', 10, 'Café de tipo americano', '2019-03-07', 1, 4),
(18, 'Café Capuccino', 13, 'Café con leche', '2019-03-07', 1, 4),
(19, 'Café Expresso', 6, 'Shot de café', '2019-03-07', 0, 4),
(20, 'Jugo de naranja', 10, 'Jugo de naranja fresco', '2019-03-07', 1, 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Fruit`
--
ALTER TABLE `Fruit`
  ADD PRIMARY KEY (`name`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `Productos`
--
ALTER TABLE `Productos`
  ADD PRIMARY KEY (`idProducto`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Productos`
--
ALTER TABLE `Productos`
  MODIFY `idProducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
