<?php
    require_once("util.php");
    session_start();

    if(isset($_SESSION['nombre'])) {
      header_html("Lab 13 - sesiones php", $_SESSION['loggedin']);
      mainView_html($_SESSION['nombre']);
      include("../html/_footer.html");
    }
    else {
      header("location: index.php");
    }

?>
