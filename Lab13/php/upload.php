<?php

  require_once("util.php");
  session_start();

  if(isset($_SESSION['nombre'])) {
    header_html("Lab 13 - subir archivo php");
    $message = "";

    $target_dir = "../uploads/";
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    // Check if image file is a actual image or fake image
    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if($check !== false) {
            $message .= "El archivo es una imagen - " . $check["mime"] . ". <br>";
            $uploadOk = 1;
        } else {
            $message .= "El archivo no es una imagen. <br>";
            $uploadOk = 0;
        }
    }

    // Check if file already exists
    if (file_exists($target_file)) {
        $message .= "El archivo ya existe, sube uno diferente. <br>";
        $uploadOk = 0;
    }

    // Check file size
    if ($_FILES["fileToUpload"]["size"] > 500000) {
        $message .= "El archivo es muy grande, el tamaño máximo es 500KB. <br>";
        $uploadOk = 0;
    }

    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
        $message .= "Solo se permiten formatos: JPG, JPEG, PNG & GIF. <br>";
        $uploadOk = 0;
    }

    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        $message .= "¡Error! El archivo no se subió. <br>";
        $button_name = "Intenta de nuevo";
        $color = "red";
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            $message .= "El archivo ". basename( $_FILES["fileToUpload"]["name"]). " se subió con éxito.";
            $button_name = "Ok";
            $color = "green";
        } else {
            $message .= "Hubo un error al subir el archivo.";
            $button_name = "Intenta de nuevo";
            $color = "red";
        }
    }

    uploadView_html($_SESSION['nombre'], $message, $button_name, $color);
    include("../html/_footer.html");
  }
  else {
    header("location: index.php");
  }
?>
