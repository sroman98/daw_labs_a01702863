<?php
    session_start();

    if(isset($_SESSION["nombre"])) {
        header("Location: main_controller.php");
    } else {
        header("Location: lab13_controller.php");
    }
?>
