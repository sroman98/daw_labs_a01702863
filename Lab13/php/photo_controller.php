<?php
  require_once("util.php");
  session_start();

  if(isset($_SESSION['nombre'])) {
    header_html("Lab 13 - ver archivo php");
    $photos = "";

    $directorio = opendir("../uploads");
    while($archivo = readdir($directorio)) {
      if($archivo != "." && $archivo != ".." && $archivo != ".DS_Store") {
          $photos.= "<img src='../uploads/".$archivo."'>";
      }
    }

    if($photos == "") {
      $photos = "No has subido fotos :(";
    }

    $photos.= "<br><br>";

    photosView_html($_SESSION['nombre'], $photos);
    include("../html/_footer.html");
  }
  else {
    header("location: index.php");
  }
?>
