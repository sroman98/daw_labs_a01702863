<?php
    require_once("util.php");
    session_start();

    //Si se ha enviado la forma
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      //Pongo el header porque ese siempre va
      header_html("Lab 13 - sesiones php");

      //Si no pone nombre o está mal el nombre carga los errores correspondientes
      if (empty($_POST["nombre"])) {
        $nombreErr = "El nombre es obligatorio";
      } else {
        $nombre = test_input($_POST["nombre"]);
        // checar que el usuario tenga solo letras y números
        if (!preg_match("/^[a-zA-Z ]*$/",$nombre)) {
          $nombreErr = "Solo se permiten letras y espacios";
        }
      }

      //Si no pone usuario o está mal el usuario carga los errores correspondientes
      if (empty($_POST["usuario"])) {
        $usuarioErr = "El usuario es obligatorio";
      } else {
        $usuario = test_input($_POST["usuario"]);
        // checar que el usuario tenga solo letras y números
        if (!preg_match("/^[a-zA-Z\d]*$/",$usuario)) {
          $usuarioErr = "Solo se permiten letras y números";
        }
      }

      //Si no pone pwd o está mal el pwd carga los errores correspondientes
      if (empty($_POST["pwd"])) {
        $pwdErr = "El password es obligatorio";
      } else {
        $pwd = test_input($_POST["pwd"]);
      }

      //Si se completó el formulario con éxito se lleva a la página de inicio
      if($nombreErr == "" && $usuarioErr == "" && $pwdErr == "" && $nombre!="" && $pwd!="" && $usuario!="") {
        $_SESSION['nombre'] = $nombre;
        $_SESSION['loggedin'] = true;
        $_SESSION['numPhotos'] = 0;
        header("Location: index.php");
        exit;
      }
      //Si no se completó, igual tiene que mostrar el formulario para mostrar los errores
      else {
        include("../html/lab13_view.html");
        include("../html/_footer.html");
      }
    }

    //Si no se envía el formulario es porque es la primera vez que se abre, inicio las con string vacío y muestro todo normal
    else {
      $nombreErr = $usuarioErr = $pwdErr = "";
      $nombre = $usuario = $pwd = "";

      header_html("Lab 13 - sesiones php");
      include("../html/lab13_view.html");
      include("../html/_footer.html");
    }

?>
