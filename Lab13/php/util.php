<?php

  function header_html($titulo="Lab 13",$loggedin=false) {
     include("../html/_header.html");
  }

  function mainView_html($nombre="No sé tu nombre") {
     include("../html/main_view.html");
  }

  function uploadView_html($nombre="No sé tu nombre", $message="No hay nada que mostrar<br>", $button_name="No hay texto para el botón", $color="no hay color") {
     include("../html/upload_view.html");
  }

  function photosView_html($nombre="No sé tu nombre", $photos="No has subido fotos :(") {
     include("../html/photos_view.html");
  }

  function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }

?>
