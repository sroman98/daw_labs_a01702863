M.AutoInit();


function getReportes(tipo) {
    $.get("reportes.php", { reporte: tipo })
        .done(function( data ) {
           var ajaxResponse = document.getElementById('reporte');
           ajaxResponse.innerHTML = data;
           ajaxResponse.style.visibility = "visible";
        }
    );
}
    