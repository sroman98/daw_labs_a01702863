<?php
    session_start();
    include_once 'util.php';
    
    $error="";
    
    if(isset($_SESSION['usuario'])) {
        include '../html/partials/_header.html';
        include '../html/principal.html';
        include '../html/partials/_footer.html';
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['codigo'])) {
            $codigo = htmlspecialchars($_POST['codigo']);
            if(insertarRegistro($_SESSION['usuario'], $codigo)) {
                echo "<script>M.toast({html: 'Registro: SUCCESS!', classes: 'rounded', displayLength:3000});</script>";
            }
            else {
                echo "<script>M.toast({html: 'Registro: SYSTEM FAILURE', classes: 'rounded', displayLength:3000});</script>";
            }
        }
    } else {
        header('location: php/principal.php');
    }
?>