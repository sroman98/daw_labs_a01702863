<?php
    session_start();
    include_once 'util.php';
    
    $error="";
    
    if(isset($_SESSION['usuario'])) {
        header('location: php/principal.php');
    } else if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['usuario']) && isset($_POST['contrasena'])){
        $usuario=htmlspecialchars($_POST['usuario']);
        $_POST['contrasena']=htmlspecialchars($_POST['contrasena']);
        $md5pwd = md5($_POST['contrasena']);
        
        if(checarUsuario($usuario, $md5pwd)) {
            $_SESSION['usuario']=$usuario;
            header('location: principal.php');
        } else {
            $error = "<p>¡Usuario y/o contraseña incorrectos!</p>";
        }
    }
    include '../html/partials/_header.html';
    include '../html/login.html';
    include '../html/partials/_footer.html';
?>