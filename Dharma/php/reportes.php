<?php
    session_start();
    include_once 'util.php';
    
    $reporte="";
    $tituloReporte="";
    
    if(isset($_SESSION['usuario'])) {
        if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['reporte'])){
            $_GET['reporte'] = htmlspecialchars($_GET['reporte']);
            echo getTituloReporte($_GET['reporte']);
            echo getReporte($_GET['reporte']);
        }  else {
            include '../html/partials/_header.html';
            include '../html/reportes.html';
            include '../html/partials/_footer.html';
        }
    } else {
         header('location: ../index.php');
    }
   
?>