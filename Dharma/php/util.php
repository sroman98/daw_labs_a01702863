<?php
    function connectDB() {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "Dharma";
    
        $con = mysqli_connect($servername, $username, $password, $dbname);
    
        if(!$con) {
          die("Connection failed: ".mysqli_connect_error());
        }
    
        return $con;
    }

    function closeDB($con) {
        mysqli_close($con);
    }
    
    function checarUsuario($usuario, $contrasena) {
        $con = connectDB();
        
        $usuario = $con->real_escape_string($usuario);
        $contrasena = $con->real_escape_string($contrasena);
        
        $query="SELECT * FROM Usuarios WHERE usuario='".$usuario."' AND contrasena='".$contrasena."'";
        
        $result = mysqli_query($con, $query);

        closeDB($con);
        
        if(mysqli_num_rows($result) > 0) {
            return true;
        }
    
        return false;
    }
    
    function getMensaje($codigo) {
        $con = connectDB();
        
        $codigo = $con->real_escape_string($codigo);
        
        $query="SELECT mensaje FROM Codigos WHERE codigo='".$codigo."'";
        
        $result = mysqli_query($con, $query);

        closeDB($con);
        
        if(mysqli_num_rows($result) > 0) {
            $mensaje = mysqli_fetch_assoc($result);
            return $mensaje['mensaje'];
        }
    
        return 'SYSTEM FAILURE';
    }
    
    function insertarRegistro($usuario="", $codigo="") {
        $con = connectDB();
        
        $usuario = $con->real_escape_string($usuario);
        $codigo = $con->real_escape_string($codigo);
        $mensaje = getMensaje($codigo);

        $query="CALL insertarRegistro ('".$usuario."', '".$codigo."', '".$mensaje."')";
        $results=mysqli_query($con,$query);
    
        closeDB($con);
        
        if($mensaje == 'SYSTEM FAILURE') {
            return false;
        }
        return true;
    }
    
    function obtenerRegistros(){
        $db = connectDB();
        
        $query="SELECT * FROM TotalRegistros";
        $results=mysqli_query($db,$query);
        
        closeDB($db);
        if(mysqli_num_rows($results) > 0) {
            return getTablaRegistros($results);
        }
        return "Por el momento no hay datos para generar tu reporte.";
    }
    
    function getTablaRegistros($results) {
        $table = "<table class='centered'><thead><tr><th>Fecha | Hora</th><th>Usuario</th><th>Código</th><th>Mensaje</th></tr></thead><tbody>";
        while($row = mysqli_fetch_assoc($results)) {
            $table.="<tr><td>".$row['fechaHora']."</td><td>".$row['usuario']."</td><td>".$row['codigoR']."</td><td>".$row['mensajeR']."</td></tr>";
        }
        $table.="</tbody></table>";
        
        return $table;
    }
    
    function obtenerFailures(){
        $db = connectDB();
        
        $query="SELECT * FROM TotalFailures";
        $results=mysqli_query($db,$query);
        
        closeDB($db);
        if(mysqli_num_rows($results) > 0) {
            return getTablaFailures($results);
        }
        return "Por el momento no hay datos para generar tu reporte.";
    }
    
    function getTablaFailures($results) {
        $table = "<table class='centered'><thead><tr><th>Fecha | Hora</th><th>Usuario</th><th>Código</th></tr></thead><tbody>";
        while($row = mysqli_fetch_assoc($results)) {
            $table.="<tr><td>".$row['fechaHora']."</td><td>".$row['usuario']."</td><td>".$row['codigoR']."</td></tr>";
        }
        $table.="</tbody></table>";
        
        return $table;
    }
    
    function obtenerSuccessVSFailure(){
        $db = connectDB();
        
        $query="SELECT * FROM SuccessVSFailure";
        $results=mysqli_query($db,$query);
        
        closeDB($db);
        if(mysqli_num_rows($results) > 0) {
            return getTablaSuccessVSFailure($results);
        }
        return "Por el momento no hay datos para generar tu reporte.";
    }
    
    function getTablaSuccessVSFailure($results) {
        $table = "<table class='centered'><thead><tr><th>Mensaje</th><th>Incidencias</th></thead><tbody>";
        while($row = mysqli_fetch_assoc($results)) {
            $table.="<tr><td>".$row['mensajeR']."</td><td>".$row['Incidencias']."</td></tr>";
        }
        $table.="</tbody></table>";
        
        return $table;
    }
    
    function getReporte($reporte) {
        switch ($_GET['reporte']) {
            case 'total':
                return obtenerRegistros();
            case 'fail':
                return obtenerFailures();
            case 'contar':
                return obtenerSuccessVSFailure();
            default:
                return "Tu reporte no puede ser ejecutado, no se encuentra dentro de las opciones del sistema";
        }
    }
    
    function getTituloReporte($reporte) {
        switch ($_GET['reporte']) {
            case 'total':
                return "<h3 class='center'>Registros Totales</h3>";
            case 'fail':
                return "<h3 class='center'>Registros 'SYSTEM FAILURE'</h3>";
            case 'contar':
                return "<h3 class='center'>Success / System Failure</h3>";
            default:
                return "";
        }
    }
?>